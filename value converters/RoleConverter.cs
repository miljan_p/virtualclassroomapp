﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.value_converters
{
    public class RoleConverter : IValueConverter
    {
       // [ValueConversion(typeof(RegisteredUser), typeof(string))]
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is Assistant)
            {
                return "Assistant";
            } else if (value is Professor)
            {
                return "Professor";
            } else if (value is Administrator)
            {
                return "Administrator";
            }  else if (value is DataGrid)
            {
                return "asfasf";
            }
            
            else
            {
                return "Unspecified";
            }
     
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
