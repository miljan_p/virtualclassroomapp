﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Telerik.Windows.Controls;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.model;


namespace VirtualClassroomApp.value_converters
{
    public class StartAppointmentConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            //return ((Appointment)value).Period.Start;
            //RadDateTimePicker p = (RadDateTimePicker)parameter;
            

            return ((Period)value).Start;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
