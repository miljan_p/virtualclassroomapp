﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp
{
    public class InstitutionNameConverter : IValueConverter
    {
        
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Institution inst = (Institution) DataObjectMapper.institutions[(int)value];

            return inst.Name;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
