﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.value_converters
{
    public class ProfessorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
         
            int i = (int)value;
            if (i == -1) { return "No assigned professor"; }
            else
            {
                Professor p = (Professor)DataObjectMapper.professors[i];
                if (!p.Active) return "";
                return p.Name + " " + p.LastName;
            }
            
           
             
            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
