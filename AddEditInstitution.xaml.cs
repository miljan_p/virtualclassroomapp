﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.DAL;
using VirtualClassroomApp.DAO;
using VirtualClassroomApp.model;
using MessageBox = System.Windows.MessageBox;

namespace VirtualClassroomApp
{
    /// <summary>
    /// Interaction logic for AddEditInstitution.xaml
    /// </summary>
    public partial class AddEditInstitution : Window
    {

        public enum Status { ADD, EDIT }
        Status status;
        private Institution institution;

        public AddEditInstitution(Institution institution, Status status = Status.ADD)
        {
            InitializeComponent();

            this.institution = institution;
            this.status = status;

            tbName.DataContext = institution;
            tbAddress.DataContext = institution;

        }

        private void Button_Click_OK(object sender, RoutedEventArgs e)
        {
            
            if (status == Status.ADD)
            {
                if (institution.Name.Equals("") || institution.Address.Equals(""))

                {
                    MessageBox.Show("Please provide all the required data for creating new institution.");
                }
                else
                {

                    this.DialogResult = true;
                    institution.SetId(DataObjectMapper.GenerateId(DataObjectMapper.institutions));
                    institution.Active = true;              
                    institution.Classrooms = new List<Classroom>();
                    institution.Users = new List<RegisteredUser>();

                    DataObjectMapper.institutions.Add(institution.GetId(), institution);
                    //FileTextIO.Add(institution, Institution.PATH);
                    InstitutionDAL.AddInstitution(institution);

                    MessageBox.Show("You have added " + institution.Name + ".");
                    this.Close();
                }

             
            } else if (status == Status.EDIT)
            {

                if (institution.Name.Equals("") || institution.Address.Equals(""))
                {
                    MessageBox.Show("Please provide correct data for editing institution.");
                } else
                {
                    // FileTextIO.Edit(institution, Institution.PATH);
                    InstitutionDAL.UpdateInstitution(institution);
                    this.DialogResult = true;
                    this.Close();
                }
                

            }

        }

        
    }
}
