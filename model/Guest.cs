﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualClassroomApp.model
{
    public class Guest
    {
        private string info;
        public Guest()
        {
            this.info = "i am guest";
        }

        public string Info { get => info; set => info = value; }
    }
}
