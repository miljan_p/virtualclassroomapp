﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualClassroomApp.model
{
    public interface IScheduler<in T>
    {
        public bool Schedule(T t);
    }
}
