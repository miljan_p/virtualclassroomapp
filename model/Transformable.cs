﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VirtualClassroomApp.model
{
    public interface Transformable

    {

        public Transformable ReadFromFile(string line);
        public string WriteToFile();
        public int GetId();

        //public static int GenerateId(Dictionary<int, Transformable> mapa)
        //{
        //    if (!(mapa.Count() == 0)) return mapa.Keys.Max() + 1;
        //    return 0;
        //}

    }
}
