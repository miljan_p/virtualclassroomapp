﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using VirtualClassroomApp.controller;

namespace VirtualClassroomApp.model
{
    [Serializable]
    public class Appointment : Transformable, IElementAppointable, INotifyPropertyChanged, ICloneable
    {
     
        public const string PATH = "appointments.txt";

        private int id;
        private bool active;
        private Period period;
        private Classroom classroom; //Institution object can be easily reached through Classroom object
        private TeachingForm teachingForm;
        private RegisteredUser user;

        private DateTime startDate;
        private TimeSpan timeSpan;
        private string groupName;

        public Appointment(int id, bool active, Period period, Classroom classroom, TeachingForm teachingForm, RegisteredUser user)
        {
            this.id = id;
            this.active = active;
            this.period = period;
            this.classroom = classroom;
            this.teachingForm = teachingForm;
            this.user = user;

        }

        public Appointment() : this(-1, false, null, null, TeachingForm.Unspecified, null) { }
       

        public Transformable ReadFromFile(string line)
        {
            string[] appointmentParts = line.Split('|');

            Appointment newAppointment = new Appointment();

            newAppointment.SetId(Int32.Parse(appointmentParts[0]));
            newAppointment.Active = bool.Parse(appointmentParts[1]);
            newAppointment.Period = (Period)DataMapper.periods[Int32.Parse(appointmentParts[2])];
            newAppointment.Classroom = (Classroom)DataMapper.classrooms[Int32.Parse(appointmentParts[3])];
            newAppointment.SetTeachingForm((TeachingForm)Enum.Parse(typeof(TeachingForm), appointmentParts[4]));
            newAppointment.User = SetUser(newAppointment.GetTeachingForm(), appointmentParts[5]);       
            newAppointment.startDate = newAppointment.Period.Start;
            newAppointment.TimeSpan = newAppointment.Period.End.Subtract(newAppointment.Period.Start);
            newAppointment.GroupName = "Classroom number: " + newAppointment.Classroom.ClassroomNumber.ToString();
            
            return (Transformable)newAppointment;

        }



        private RegisteredUser SetUser(TeachingForm tf, string user)
        {
            int userId = Int32.Parse(user);


            switch (tf)
            {
                case TeachingForm.Lecture:

                    return (Professor)DataMapper.professors[userId];

                case TeachingForm.Exercise:
                    return (Assistant)DataMapper.assistants[userId];

                case TeachingForm.Unspecified:
                    break;

                default: return (Administrator)DataMapper.administrators[userId];
            }

            return null;
        }





        public int GetId()
        {
            return this.id;
        }

        public void SetId(int id)
        {
            this.id = id;
        }

        public string WriteToFile()
        {

            string[] array = { id.ToString(), active.ToString(), period.GetId().ToString(), classroom.GetId().ToString(), teachingForm.ToString(), user.GetId().ToString() };

            return String.Join("|", array) + "\n";
        }
        public bool Active { get { return active; } set { active = value; OnPropertyChanged("Active"); } }
        public Period Period { get { return period;} set { period = value; OnPropertyChanged("Active"); } }
        public RegisteredUser User { get { return user; } set { user = value; OnPropertyChanged("User"); } }
        public Classroom Classroom { get { return classroom; } set { classroom = value; OnPropertyChanged("Classroom"); } }


        

        public DateTime StartDate { get => startDate; set => startDate = value; }
        public TimeSpan TimeSpan { get => timeSpan; set => timeSpan = value; }
        public string GroupName { get => groupName; set => groupName = value; }


        public bool ScheduledBy(IScheduler<Appointment> scheduler)
        {
            
            if (scheduler.Schedule(this)) return true;
            else return false;
        }

        public TeachingForm GetTeachingForm()
        {
            return this.teachingForm;
        }

        public void SetTeachingForm(TeachingForm value)
        {
            teachingForm = value;
        }


        public Period GetPeriod()
        {
            return this.period;
        }


        public Classroom GetClassroom()
        {
            return this.classroom;
        }


        private void recalculate()
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public object Clone()
        {

            Appointment appointmentCopy = new Appointment();
            appointmentCopy.SetId(this.GetId());
            appointmentCopy.Active = this.Active;
            appointmentCopy.Period = this.Period;
            appointmentCopy.Classroom = this.Classroom;
            appointmentCopy.SetTeachingForm(this.GetTeachingForm());
            appointmentCopy.User = this.User;

            appointmentCopy.StartDate = this.StartDate;
            appointmentCopy.TimeSpan = this.TimeSpan;
            appointmentCopy.GroupName = this.GroupName;


           return appointmentCopy;
        }
    }
}
