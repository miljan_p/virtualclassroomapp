﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using VirtualClassroomApp.controller;

namespace VirtualClassroomApp.model
{
    [Serializable]
    public class Institution : Transformable, ICloneable, INotifyPropertyChanged
    {
        public const string PATH = "institutions.txt";
        public const string BIN_PATH = "institutions.bin";

        private int id;
        private bool active;
        private string name;
        private string address;
        private List<Classroom> classrooms;
        private List<RegisteredUser> users;

       

        public Institution(int id, bool active, string name, string address, List<Classroom> classrooms, List<RegisteredUser> users)
        {
            this.id = id;
            this.active = active;
            this.name = name;
            this.address = address;
            this.classrooms = classrooms;
            this.users = users;
        }

        public Institution() : this(-1, false, "", "", null, null)
        {
        }


       // public bool Active {  get => active; set => active = value; }
       // public string Name { get => name; set => name = value; }

        public bool Active { get { return active; } set { active = value; OnPropertyChanged("Active"); } }
       

        public string Name { get { return name; } set { name = value; OnPropertyChanged("Name"); } }
       
        public string Address { get { return address; } set { address = value; OnPropertyChanged("Address"); } }


        public List<Classroom> Classrooms { get => classrooms; set => classrooms = value; }
        public List<RegisteredUser> Users { get => users; set => users = value; }

        public int GetId()
        {
            return this.id;
        }

        public void SetId(int id)
        {
            this.id = id;
        }

        public Transformable ReadFromFile(string line)
        {

            string[] institutionParts = line.Split('|');

            Institution newInstitution = new Institution();

            newInstitution.SetId(Int32.Parse(institutionParts[0]));
            newInstitution.Active = bool.Parse(institutionParts[1]);
            newInstitution.Name = institutionParts[2];
            newInstitution.Address = institutionParts[3];
            newInstitution.Classrooms = DataMapper.FindClassroomsForInstitution(newInstitution.GetId());
            newInstitution.Users = DataMapper.FindUsersForInstitution(newInstitution.GetId());


            return (Transformable)newInstitution;

        }

        public string WriteToFile()
        {
            
            string[] array = { id.ToString(), active.ToString(), Name, Address };
            return String.Join("|", array) + "\n";
           
        }


        public object Clone()
        {
            Institution copyInstitution = new Institution();

            copyInstitution.SetId(this.GetId());
            copyInstitution.Active = this.Active;
            copyInstitution.Name = this.Name;
            copyInstitution.Address = this.Address;
            copyInstitution.Classrooms = this.Classrooms;
            copyInstitution.Users = this.Users;

            return copyInstitution;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

       
    }
}
