﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using VirtualClassroomApp.controller;

namespace VirtualClassroomApp.model
{
    [Serializable]
    public class Professor : RegisteredUser, IScheduler<IElementAppointable>, INotifyPropertyChanged
    {
        public const string PATH = "professors.txt";
        public const string BIN_PATH = "professors.bin";

        private List<Assistant> assistants;
        private int institution;

        public Professor(int id, bool active, string name, string lastName, string email, string username, string password, ScheduleBehavior scheduleBehavior, List<Assistant> assistants, int institution)
            : base(id, active, name, lastName, email, username, password, scheduleBehavior)
        {
            this.assistants = assistants;
            this.institution = institution;
        }

        public List<Assistant> Assistants { get { return assistants; } set { assistants = value; OnPropertyChanged("Assistants"); } }
        public int Institution { get { return institution; } set { institution = value; OnPropertyChanged("Institution"); } }


        public Professor() : this(-1, false, "", "", "", "", "", new ScheduleObject(), null, -1) { }
       
        public override Transformable ReadFromFile(string line)
        {

            string[] userParts = line.Split('|');

            Professor user = new Professor();

            user.SetId(Int32.Parse(userParts[0]));
            user.Active = bool.Parse(userParts[1]);
            user.Name = userParts[2];
            user.LastName = userParts[3];
            user.Email = userParts[4];
            user.Username = userParts[5];
            user.Password = userParts[6];         
            user.ScheduleBehavior = new ScheduleObject();
            user.Assistants = DataMapper.FindAssistantsForProfessor(user.GetId());
            user.Institution = Int32.Parse(userParts[7]);

            return (Transformable)user;


        }

        public override string WriteToFile()
        {
            return base.WriteToFile() + "|" +this.Institution.ToString() +"\n";
        }

        public bool Schedule(IElementAppointable elements)
        {

            Period p = elements.GetPeriod();
            Classroom cl = elements.GetClassroom();
            TeachingForm tf = elements.GetTeachingForm();
            // Create appointment logic (use Strategy pattern (Single Responsibility Object) behavior object)
            return this.scheduleBehavior.Schedule(this, cl, p, tf);
        }

        public override string ToString()
        {
            return this.Name + " " + this.LastName;
        }

        
      
    }

}
