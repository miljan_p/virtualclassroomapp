﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualClassroomApp.model
{
    [Serializable]
    class Administrator : RegisteredUser, IScheduler<IElementAppointable>
    {
        public const string PATH = "administrators.txt";
        public const string BIN_PATH = "assistants.bin";

        public Administrator(int id, bool active, string name, string lastName, string email, string username, string password, ScheduleBehavior scheduleBehavior)
            : base(id, active, name, lastName, email, username, password, scheduleBehavior)
        {

        }

        public Administrator() : this(-1, false, "", "", "", "", "", null)
        {      

        }


        public override Transformable ReadFromFile(string line)
        {

            string[] userParts = line.Split('|');

            Administrator user = new Administrator();

            user.SetId(Int32.Parse(userParts[0]));
            user.Active = bool.Parse(userParts[1]);
            user.Name = userParts[2];
            user.LastName = userParts[3];
            user.Email = userParts[4];
            user.Username = userParts[5];
            user.Password = userParts[6];
            //user.Institution = Int32.Parse(userParts[7]);
            user.ScheduleBehavior = new ScheduleObject();

            return (Transformable)user;


        }

        public override string WriteToFile()
        {
            return base.WriteToFile() + "\n";
        }

        public bool Schedule(IElementAppointable elements)
        {

            Period p = elements.GetPeriod();
            Classroom cl = elements.GetClassroom();
            TeachingForm tf = elements.GetTeachingForm();
            // Create appointment logic (use Strategy pattern (Single Responsibility Object) behavior object)
            return this.scheduleBehavior.Schedule(this, cl, p, tf);
        }
    }
}
