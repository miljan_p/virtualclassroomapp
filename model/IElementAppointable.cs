﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualClassroomApp.model
{
    public interface IElementAppointable
    {
        public Period GetPeriod();
        public Classroom GetClassroom();
        public TeachingForm GetTeachingForm();
    }
}
