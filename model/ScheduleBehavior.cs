﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualClassroomApp.model
{
    public interface ScheduleBehavior
    {
        public bool Schedule(RegisteredUser user, Classroom classroom, Period period, TeachingForm tf);

    }
}
