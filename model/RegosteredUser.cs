﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.DAL;

namespace VirtualClassroomApp.model
{
    [Serializable]
    public abstract class RegisteredUser : Transformable, INotifyPropertyChanged
    {
        protected int id;
        protected bool active;
        protected string name;
        protected string lastName;
        protected string email;
        protected string username;
        protected string password;
        //protected int institution;
        protected ScheduleBehavior scheduleBehavior;

       

        public RegisteredUser(int id, bool active, string name, string lastName, string email, string username, string password,  ScheduleBehavior scheduleBehavior)
        {
            this.id = id;
            this.active = active;
            this.name = name;
            this.lastName = lastName;
            this.email = email;
            this.username = username;
            this.password = password;
            //this.institution = institution;
            this.ScheduleBehavior = scheduleBehavior;
        }    

        public RegisteredUser() : this(-1, false, "", "", "", "", "", null) { }

        public bool Active { get { return active; } set { active = value; OnPropertyChanged("Active"); } }
        public string Name { get { return name; } set { name = value; OnPropertyChanged("Name"); } }
        public string LastName { get { return lastName; } set { lastName = value; OnPropertyChanged("LastName"); } }
        public string Email { get { return email; } set { email = value; OnPropertyChanged("Email"); } }
        public string Username { get { return username; } set { username = value; OnPropertyChanged("Username"); } }
        public string Password { 
            get { return password; }
            set 
            {
                //if (passwordIsUnique(value))
               // if (RegisteredUserDAL.IsUniquPassword(value, this.in))
                    password = value;
                    OnPropertyChanged("Password");
                
                 
            } 
        
        
        }
        //public int Institution { get => institution; set => institution = value; }
        public ScheduleBehavior ScheduleBehavior { get => scheduleBehavior; set => scheduleBehavior = value; }


        public int GetId() { return this.id; }
       

        public void SetId(int value) { this.id = value; }
       

        public virtual string WriteToFile()
        {
            string[] array = { id.ToString(), active.ToString(), name, lastName, email, username, password };
            return String.Join("|", array);
        }
        public abstract Transformable ReadFromFile(string line);

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        private bool passwordIsUnique(String password)
        {
            
            if (DataMapper.AssistantsToList().Exists(a => a.Password.Equals(password))) return false;
            if (DataMapper.ProfessorsToList().Exists(a => a.Password.Equals(password))) return false;
            return true;
        }

    } //od klase

}

