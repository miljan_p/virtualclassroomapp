﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.DAL;
using VirtualClassroomApp.DAO;

namespace VirtualClassroomApp.model
{
    [Serializable]
    public class ScheduleObject : ScheduleBehavior
    {

        public ScheduleObject()
        {

        }
        public bool Schedule(RegisteredUser user, Classroom classroom, Period period, TeachingForm tf)
        {
            //appointing logic

            if (DataObjectMapper.avaliable(classroom, period, user.GetId()))
            {

                //ADD ALL CREATED OBJECT DURING THIS PROCESS TO APPROPRIATE PLACES (files and hash maps)

                Appointment newAppointment = new Appointment();
                newAppointment.SetId(DataObjectMapper.GenerateId(DataObjectMapper.appointments));
                newAppointment.Active = true;
                newAppointment.Period = period;
                period.Active = true;
                period.SetId(DataObjectMapper.GenerateId(DataObjectMapper.periods));
                newAppointment.Classroom = classroom;
                if (user is Professor) newAppointment.SetTeachingForm(TeachingForm.Lecture);
                else newAppointment.SetTeachingForm(TeachingForm.Exercise);

                newAppointment.User = user;

                newAppointment.GroupName = "Classroom number: " + newAppointment.Classroom.ClassroomNumber.ToString();
                newAppointment.StartDate = newAppointment.Period.Start;
                newAppointment.TimeSpan = newAppointment.Period.End.Subtract(newAppointment.Period.Start);

                DataObjectMapper.appointments.Add(newAppointment.GetId(), newAppointment);
                DataObjectMapper.periods.Add(newAppointment.Period.GetId(), newAppointment.Period);
                
                AppointmentDAL.AddAppointment(newAppointment);
               

                return true;
            }

            return false;
        }

       

    }

}

