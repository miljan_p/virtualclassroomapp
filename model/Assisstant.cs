﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace VirtualClassroomApp.model
{
    [Serializable]
    public class Assistant : RegisteredUser, IScheduler<IElementAppointable>, INotifyPropertyChanged

    {
        public const string PATH = "assistants.txt";
        public const string BIN_PATH = "assistants.bin";

        private int institution;
        private int professor;

        public Assistant(int id, bool active, string name, string lastName, string email, string username, string password,  ScheduleBehavior scheduleBehavior, int professor, int institution)
            : base(id, active, name, lastName, email, username, password,  scheduleBehavior)
        {
            this.professor = professor;
            this.institution = institution;
        }

        public int Professor { get { return professor; } set { professor = value; OnPropertyChanged("Professor"); } }

        public int Institution { get { return institution; } set { institution = value; OnPropertyChanged("Institution"); } }

        public Assistant() : this(-1, false, "", "", "", "", "", new ScheduleObject(), -1, -1) { }

        public override Transformable ReadFromFile(string line)
        {

            string[] userParts = line.Split('|');

            Assistant user = new Assistant();

            user.SetId(Int32.Parse(userParts[0]));
            user.Active = bool.Parse(userParts[1]);
            user.Name = userParts[2];
            user.LastName = userParts[3];
            user.Email = userParts[4];
            user.Username = userParts[5];
            user.Password = userParts[6];
            user.ScheduleBehavior = new ScheduleObject();
            
   
            if (userParts[7] == "") user.Professor = -1;
            else user.Professor = Int32.Parse(userParts[7]);

            user.Institution = Int32.Parse(userParts[8]);

            return (Transformable)user;
        }

        public override string WriteToFile()
        {
            return base.WriteToFile() + "|" + this.Professor.ToString() + "|" + this.Institution.ToString() +"\n";
        }

        public bool Schedule(IElementAppointable elements)
        {
            Period p = elements.GetPeriod();
            Classroom cl = elements.GetClassroom();
            TeachingForm tf = elements.GetTeachingForm();
            //Create appointment logic (use Strategy pattern (Single Responsibility Object); behavior object)
            return this.scheduleBehavior.Schedule(this, cl, p, tf);
        }

        //public event PropertyChangedEventHandler PropertyChanged;

        //protected void OnPropertyChanged(String propertyName)
        //{
        //    if (PropertyChanged != null)
        //    {
        //        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}
    }
}
