﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using VirtualClassroomApp.utility;

namespace VirtualClassroomApp.model
{
    [Serializable]
    public class Period : Transformable, INotifyPropertyChanged 
    {
        public const string PATH = "periods.txt";
        public const string BIN_PATH = "periods.bin";

        private int id;
        private bool active;
        private DateTime start;
        private DateTime end;

        

        public Period(int id, bool active, DateTime start, DateTime end)
        {
            this.id = id;
            this.active = active;
            this.start = start;
            this.end = end;
        }

        public Period() : this(-1, false, DateTime.Now, DateTime.Now)
        {
        }

        public int GetId() { return this.id; }

        public void SetId(int id) { this.id = id; }

        public bool Active { get { return active; } set { active = value; OnPropertyChanged("Active"); } }

        public DateTime Start { get { return start; } set { start = value; OnPropertyChanged("Start"); } }

        public DateTime End { get { return end; } set { end = value; OnPropertyChanged("End"); } }

       



        public Transformable ReadFromFile(string line)
        {
            string[] periodParts = line.Split('|');

            Period newPeriod = new Period();

            newPeriod.SetId(Int32.Parse(periodParts[0]));
            newPeriod.Active = bool.Parse(periodParts[1]);
            newPeriod.Start = Utility.createDateWithTime(periodParts[2]);
            newPeriod.End = Utility.createDateWithTime(periodParts[3]);

            return (Transformable)newPeriod;

        }

        public string WriteToFile()
        {

            var formattedStartDate = this.Start.ToString("dd-MM-yyyy HH:mm");
            var formattedEndDate = this.End.ToString("dd-MM-yyyy HH:mm");

            string[] array = { id.ToString(), active.ToString(), formattedStartDate, formattedEndDate };

            return String.Join("|", array) + "\n";
        }



        public bool Overlaps(Period other)
        {
            if (this.Start < other.Start && this.End < other.Start) return false;
            else if (this.Start > other.End && this.End > other.End) return false;
            else return true;

        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        
    }
}
