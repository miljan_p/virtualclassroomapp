﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace VirtualClassroomApp.model
{
    public class Product : ViewModelBase

    {
        
        private ObservableCollection<Appointment> appointments;
        private DateTime periodStart;
        private DateTime periodEnd;

        public Product(ObservableCollection<Appointment> appointments, DateTime periodStart, DateTime periodEnd)
        {
            //this.appointments = getAppointments(appointments);
            this.appointments = appointments;
            this.periodStart = periodStart;
            this.periodEnd = periodEnd;
        }

        public ObservableCollection<Appointment> Appointments { get => appointments; set => appointments = value; }
        public DateTime PeriodStart { get => periodStart; set => periodStart = value; }
        public DateTime PeriodEnd { get => periodEnd; set => periodEnd = value; }

        private ObservableCollection<Appointment> getAppointments(List<Appointment> appointments)
        {
            return new ObservableCollection<Appointment>(appointments);
        }
    }
}
