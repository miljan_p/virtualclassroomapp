﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.DAL;
using VirtualClassroomApp.Validations;

namespace VirtualClassroomApp.model
{
    [Serializable]
    public class Classroom : Transformable, ICloneable, INotifyPropertyChanged //, IEditableObject
    {

        public const string PATH = "classrooms.txt";
        public const string BIN_PATH = "assistants.bin";

        private int id;
        private bool active;
        private int classroomNumber;
        private int size;
        private bool computers;
        private int institution;

        public int GetId()
        {
            return this.id;
        }

        public void SetId(int id)
        {
            this.id = id;
        }


        public bool Active
        {
            get { return active; }
            set { active = value; OnPropertyChanged("Active"); }

        }

        
        public int ClassroomNumber
        {
            get { return classroomNumber; }
            
            set 
            {
               // if (value == int.MinValue) 
               // if (string.IsNullOrEmpty(value) ||string.IsNullOrWhiteSpace(value))
                if (!ClassroomDAL.IsUniqeClrNumber(this.GetId(), this.Institution, value)) throw new ArgumentException("djuraaaa");
                classroomNumber = value; OnPropertyChanged("ClassroomNumber");
                //var validacijaNumber = new ClassroomNumberValidation();


                //if (DataObjectMapper.institutions != null)
                //{
                //    if (this.GetId() == -1)
                //    {
                //        //znaci dodajem novu
                //        if (!DataObjectMapper.ClassroomNumberExist2(value, this.Institution, this.GetId()) && validacijaNumber.Validate(this, null).IsValid)
                //        {

                //               classroomNumber = value;
                //               OnPropertyChanged("ClassroomNumber");

                //        }

                //    }
                //    else if (DataObjectMapper.classrooms.ContainsKey(this.GetId())){
                //        //znaci editujem

                //    }
                //    //if (!DataObjectMapper.ClassroomNumberExist2(value, this.Institution, this.GetId()) && validacijaNumber.Validate(this, null).IsValid)
                //    //{

                //    //    classroomNumber = value;
                //    //    OnPropertyChanged("ClassroomNumber");

                //    //}
                //    //else
                //    //{
                //    //    throw new ArgumentException("Classroom number already exists");
                //    //}
                //} else
                //{

                //    if (validacijaNumber.Validate(this, null).IsValid)
                //    {
                //        classroomNumber = value;
                //        OnPropertyChanged("ClassroomNumber");
                //    }
                //}





            }
        }

        public int Size
        {
            get { return size; }
            set {
                 size = value; 
                 OnPropertyChanged("Size"); 
                }
        }

        public bool Computers
        {
            get { return computers; }
            set { computers = value; OnPropertyChanged("Computers"); }
        }



        
        public int Institution { get => institution; set => institution = value; }

        //public string Error => throw new NotImplementedException();

        //public string this[string "ClassroomNumber"] => throw new NotImplementedException();

        //constructor when we are reading from a file
        public Classroom(int id, bool active, int classroomNumber, int size, bool computers, int institution)
        {

            this.id = id;
            this.Active = active;
            this.ClassroomNumber = classroomNumber;
            this.Size = size;
            this.Computers = computers;
            this.Institution = institution;
        }

        //constructor when we are creating completely new classroom i.e. we need to generate new and unique 
        //classroom number
        public Classroom(int id, bool active, int size, bool computers, int institution)
        {

            this.id = id;
            this.Active = active;
            this.ClassroomNumber = DataMapper.generateRoomNumber(institution);
            this.Size = size;
            this.Computers = computers;
            this.Institution = institution;
        }

        public Classroom(int id, bool active, int institution) 
        {
            this.id = id;
            this.active = active;
            this.institution = institution;
        }
        public Classroom() : this(-1, false,0, 0, false, -1)
        {

        }

        public Classroom(int institution) 
        {
            //this(-1, false, 0, DataMapper.generateRoomNumber(institution), -1, false, institution)
            this.SetId(-1);
            this.Active = false;
            this.ClassroomNumber = DataMapper.generateRoomNumber(institution);
            this.Size = 0;
            this.Computers = false;
            this.Institution = institution;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public Transformable ReadFromFile(string line)
        {

            string[] classroomParts = line.Split('|');

            Classroom newClassroom = new Classroom();

            newClassroom.SetId(Int32.Parse(classroomParts[0]));
            newClassroom.Active = bool.Parse(classroomParts[1]);
            newClassroom.ClassroomNumber = Int32.Parse(classroomParts[2]);
            newClassroom.Size = Int32.Parse(classroomParts[3]);
            newClassroom.Computers = bool.Parse(classroomParts[4]);
            newClassroom.Institution = Int32.Parse(classroomParts[5]);


            return (Transformable)newClassroom;

        }

        public string WriteToFile()
        {
            string[] array = { id.ToString(), active.ToString(), classroomNumber.ToString(), size.ToString(), computers.ToString(), institution.ToString() };
            return String.Join("|", array) + "\n";
        }

        public object Clone()
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return "Classroom number: " + this.ClassroomNumber.ToString();
        }

        //public void BeginEdit()
        //{
        //    throw new NotImplementedException();
        //}

        //public void EndEdit()
        //{
        //    throw new NotImplementedException();
        //}

        //public void CancelEdit()
        //{
        //    throw new NotImplementedException();
        //}
    }
}
