﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.DAL;
using VirtualClassroomApp.model;
using VirtualClassroomApp.Validations;
using static VirtualClassroomApp.AddEditInstitution;
using MessageBox = System.Windows.Forms.MessageBox;

//////


namespace VirtualClassroomApp
{
    /// <summary>
    /// Interaction logic for AdministratorWindow.xaml
    /// </summary>
    public partial class AdministratorWindow : Window
    {
        private static int idIns;
        private Institution selectedInstitution;
        private ICollectionView institutionsView;
        private ICollectionView classroomsView;
        private ICollectionView assistantsView;
        private ICollectionView professorsView;
        private ICollectionView appointmentsView;

        private ObservableCollection<Institution> institutions;
        private ObservableCollection<Classroom> classrooms;
        private ObservableCollection<Assistant > assistants;
        private ObservableCollection<Professor> professors;
        private ObservableCollection<Appointment> appointments;

        public ObservableCollection<Appointment> Appointments { get => appointments; set => appointments = value; }

        public AdministratorWindow(IScheduler<Appointment> loggedAdministrator)
        {
            InitializeComponent();
            
            this.institutions = new ObservableCollection<Institution>(DataObjectMapper.InstitutionsToList()
                                                                                .Where(i => i.Active));
            
            this.selectedInstitution = (Institution) cbInstitutions.SelectedItem;
           
            institutionsView = CollectionViewSource.GetDefaultView(institutions);
           
            cbInstitutions.ItemsSource = institutionsView;
            cbInstitutions.IsSynchronizedWithCurrentItem = true;
           // if (this.selectedInstitution == null) MessageBox.Show("null");
            idIns = this.selectedInstitution.GetId();
        }

       

        private void AddInstitution(object sender, RoutedEventArgs e)
        {
            Institution newInstitution = new Institution();
            AddEditInstitution aei = new AddEditInstitution(newInstitution);

            if (aei.ShowDialog() == true) { institutions.Add(newInstitution); }
           
        }

        private void EditInstitution(object sender, RoutedEventArgs e)
        {
            if (selectedInstitution != null)
            {
                Institution oldInstitution = (Institution) selectedInstitution.Clone();
                AddEditInstitution aei = new AddEditInstitution(selectedInstitution, Status.EDIT);

                if (aei.ShowDialog() != true) //ako je kliknu cancel, tj nije dovrsio izmenu, vracaju se starne vrednosti..
                {
                    int index = institutions.IndexOf(selectedInstitution);
                    institutions[index] = oldInstitution;

                } 
            } else
            {
                MessageBox.Show("Please select institution which you want to edit.");
            }
            
        }

        private void RemoveInstitution(object sender, RoutedEventArgs e)
        {
            if (selectedInstitution == null) { MessageBox.Show("Please select institution which you want to remove."); }
            else 
            {
                RemoveInstitution ri = new RemoveInstitution(selectedInstitution);
                if (ri.ShowDialog() == true) { institutions.Remove(selectedInstitution); }
           
            }
        }


        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.selectedInstitution = (Institution)cbInstitutions.SelectedItem;
            if (selectedInstitution != null)
            {
                idIns = this.selectedInstitution.GetId();

                this.classrooms = new ObservableCollection<Classroom>(DataObjectMapper.ClassroomsToList()
                                                                                .Where(cl => cl.Active &&
                                                                                       cl.Institution ==
                                                                                       selectedInstitution.GetId()));

                this.assistants = new ObservableCollection<Assistant>(DataObjectMapper.AssistantsToList()
                                                                                    .Where(user => user.Active &&
                                                                                          (user is Assistant) &&
                                                                                            findInstitutionId(user) == selectedInstitution.GetId()));

                this.professors = new ObservableCollection<Professor>(DataObjectMapper.ProfessorsToList()
                                                                                    .Where(user => user.Active &&
                                                                                          (user is Professor) &&
                                                                                            findInstitutionId(user) == selectedInstitution.GetId()));

                this.appointments = new ObservableCollection<Appointment>(DataObjectMapper.AppointmentsToList()
                                                                                    .Where(appointment => appointment.Active &&
                                                                                           appointment.Classroom.Institution == selectedInstitution.GetId()));

                refreshTabControl();
            }


        }

        private void Click_Logout(object sender, RoutedEventArgs e)
        {
            
            this.Close();
        }

        private void btnSchedule_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime start = (DateTime)pickerStart.SelectedValue;
                DateTime end = (DateTime)pickerEnd.SelectedValue;

                if (selectedInstitution == null)
                {
                    MessageBox.Show("Please select institution for which you want to generate schedule.");

                }
                else if (pickerStart.DateTimeText.Equals("") || pickerEnd.DateTimeText.Equals(""))
                {
                    MessageBox.Show("Please select start and end for which you want to generate schedule.");
                }
                else if (start >= end || end <= start)
                {
                    MessageBox.Show("Please select appropriate start and end dates.");
                }
                else
                {

                    ScheduleView sv = new ScheduleView(selectedInstitution, start, end, appointments, null, null);
                    sv.Show();

                    if (sv.DialogResult == true)
                    {
                        sv.Close();
                    }

                }

            }
            catch (Exception) { }         

        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var tc = sender as System.Windows.Controls.TabControl; //The sender is a type of TabControl...

                if (selectedInstitution == null)
                {
                         MessageBox.Show("Please select institution.");
                }
            
        }

        private void refreshTabControl()
        {
            dgClassrooms.DataContext = classrooms;
            classroomsView = CollectionViewSource.GetDefaultView(classrooms);
            dgClassrooms.ItemsSource = classroomsView;
            dgClassrooms.IsSynchronizedWithCurrentItem = true;

            dgAssistants.DataContext = assistants;
            assistantsView = CollectionViewSource.GetDefaultView(assistants);
            dgAssistants.ItemsSource = assistantsView;
            dgAssistants.IsSynchronizedWithCurrentItem = true;

            dgProfessors.DataContext = professors;
            professorsView = CollectionViewSource.GetDefaultView(professors);
            dgProfessors.ItemsSource = professorsView;
            dgProfessors.IsSynchronizedWithCurrentItem = true;

            dgAppointments.DataContext = appointments;
            appointmentsView = CollectionViewSource.GetDefaultView(appointments);
            dgAppointments.ItemsSource = appointmentsView;
            dgAppointments.IsSynchronizedWithCurrentItem = true;

        }

        private int findInstitutionId(RegisteredUser user)
        {
            if (user is Assistant) return ((Assistant)user).Institution;
            else return ((Professor)user).Institution;
        }

        //classroom
        private void dgClassrooms_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            try
            {
                Classroom clr = (Classroom)dgClassrooms.SelectedItem;
                var str = ((System.Windows.Controls.TextBox)e.EditingElement).Text;
                var col = getColBeingEdited(dgClassrooms);
                if (col.Equals("Computers")) { ClassroomDAL.UpdateClassroom(clr); MessageBox.Show("Classroom successfully edited."); return; }
                else if (col.Equals("Classroom number")) { if (ClassroomDAL.IsUniqeClrNumber(clr.GetId(), selectedInstitution.GetId(), int.Parse(str))) { ClassroomDAL.UpdateClassroom(clr); MessageBox.Show("Classroom successfully edited."); return; } }
                else if (col.Equals("Size")) { ClassroomDAL.UpdateClassroom(clr); MessageBox.Show("Classroom successfully edited."); }
            }
            catch (Exception) { }

        }

       
        //professor
        private void dgProfessors_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            var professor = (Professor)dgProfessors.SelectedItem;

            try
            {
                var action = (DataGridEditAction)e.EditAction;
                var str = ((System.Windows.Controls.TextBox)e.EditingElement).Text;
                var colEdited = getColBeingEdited(dgProfessors);

                switch (colEdited)
                {
                    case "Name":
                        if (str.Equals("")) { MessageBox.Show("Cannot be empty string"); }
                        else if (allSpaces(str)) { MessageBox.Show("Cannot be all empty spaces"); }
                        else { professor.Name = str; ProfessorDAL.UpdateProfessor(professor); MessageBox.Show("Professor successfully edited."); }

                        break;
                    case "LastName":
                        if (str.Equals("")) { MessageBox.Show("Cannot be empty string"); }
                        else if (allSpaces(str)) { MessageBox.Show("Cannot be all empty spaces"); }
                        else { professor.LastName = str; ProfessorDAL.UpdateProfessor(professor); MessageBox.Show("Professor successfully edited."); }

                        break;
                    case "Email":
                        if (correctEmail(str)) { professor.Email = str; ProfessorDAL.UpdateProfessor(professor); MessageBox.Show("Professor successfully edited."); }
                       
                        break;

                    case "Username":
                        // 
                        if (str.Equals("")) MessageBox.Show("Cannot be empty string");
                        else if (allSpaces(str)) { MessageBox.Show("Cannot be all empty spaces"); }
                        else { if (isUniqeUsername(professor.GetId(), str)) { professor.Username = str; ProfessorDAL.UpdateProfessor(professor); MessageBox.Show("Professor successfully edited."); } }

                        break;
                    case "Password":
                        // 
                        if (str.Equals("")) MessageBox.Show("Cannot be empty string");
                        else if (allSpaces(str)) { MessageBox.Show("Cannot be all empty spaces"); }
                        else { if (isUniqePassword(professor.GetId(), str)) { professor.Password = str; ProfessorDAL.UpdateProfessor(professor); MessageBox.Show("Professor successfully edited."); } }

                        break;

                    default: break;
                }

            }
            catch (Exception) { }
  
        }

        private void btnAssistants_Click(object sender, RoutedEventArgs e)
        {
            //btnAssistants kliknut kod Professor-a....
            //prikazuju se svi asistenti profesora, kao i dostupni asistenti, za dodavanje i uklanjanje profesorovih asistenata
            Professor p = (Professor)dgProfessors.SelectedItem;
            EditAssistantsForProfessor eafp = new EditAssistantsForProfessor(p);
            eafp.Show();

        }

        //assistant
        private void dgAssistants_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            var assistant = (Assistant)dgAssistants.SelectedItem;

            try
            {
                var action = (DataGridEditAction)e.EditAction;
                var str = ((System.Windows.Controls.TextBox)e.EditingElement).Text;
                var colEdited = getColBeingEdited(dgAssistants);

                switch (colEdited)
                {
                    case "Name":
                        if (str.Equals("")) { MessageBox.Show("Cannot be empty string"); }
                        else if (allSpaces(str)) { MessageBox.Show("Cannot be all empty spaces"); }
                        else { assistant.Name = str; AssistantDAL.UpdateAssistant(assistant); MessageBox.Show("Assistant successfully edited."); }

                        break;
                    case "LastName":
                        if (str.Equals("")) { MessageBox.Show("Cannot be empty string"); }
                        else if (allSpaces(str)) { MessageBox.Show("Cannot be all empty spaces"); }
                        else { assistant.LastName = str; AssistantDAL.UpdateAssistant(assistant); MessageBox.Show("Assistant successfully edited."); }

                        break;
                    case "Email":
                        if (correctEmail(str)) { assistant.Email = str; AssistantDAL.UpdateAssistant(assistant); MessageBox.Show("Assistant successfully edited."); }

                        break;

                    case "Username":
                        // 
                        if (str.Equals("")) MessageBox.Show("Cannot be empty string");
                        else if (allSpaces(str)) { MessageBox.Show("Cannot be all empty spaces"); }
                        else { if (isUniqeUsername(assistant.GetId(), str)) { assistant.Username = str; AssistantDAL.UpdateAssistant(assistant); MessageBox.Show("Assistant successfully edited."); } }

                        break;
                    case "Password":
                        // 
                        if (str.Equals("")) MessageBox.Show("Cannot be empty string");
                        else if (allSpaces(str)) { MessageBox.Show("Cannot be all empty spaces"); }
                        else { if (isUniqePassword(assistant.GetId(), str)) { assistant.Password = str; AssistantDAL.UpdateAssistant(assistant); MessageBox.Show("Assistant successfully edited."); } }

                        break;

                    default: break;
                }
            }
            catch (Exception) { }
        }

        private void btnProfessor_Click(object sender, RoutedEventArgs e)
        {
            //btnProfessor kliknut kod assistenta, 
            //prikazuju se svi dostupni profesori (aktivni), (ako je profesor bio od ranije obrisan, na dugme remove nece se dodavati u listu dostupnih profesora)
            //ovde ce se dodavati/uklanjati asistentov profesor..
            Assistant selectedAssistant = (Assistant) dgAssistants.SelectedItem;
            if (selectedAssistant != null)
            {
                EditProfessorForAssistant epfa = new EditProfessorForAssistant(selectedAssistant);
                epfa.Show();
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (tabClassroom.IsSelected && selectedInstitution != null)
            {
                try
                {
                    Classroom newClassroom = new Classroom(-1, true, selectedInstitution.GetId());
                    AddClassroom ac = new AddClassroom(newClassroom);
                    if (ac.ShowDialog() == true)
                    {
                        //FileTextIO.Add((Transformable)newClassroom, Classroom.PATH);
                        newClassroom.SetId(DataObjectMapper.classrooms.Keys.Max() + 1);
                        ClassroomDAL.AddClassroom(newClassroom);
                        DataObjectMapper.classrooms.Add(newClassroom.GetId(), newClassroom);
                        selectedInstitution.Classrooms.Add(newClassroom);
                        classrooms.Add(newClassroom);

                        MessageBox.Show("Classroom successfully added.");
                        classroomsView.Refresh();
                    }
                    else if (ac.DialogResult == false)
                    {
                        ac.Close();
                    }
                }
                catch (Exception ex) { }

            }
            else if (tabAssistants.IsSelected && selectedInstitution != null)
            {
                //DataObjectMapper.GenerateId(DataObjectMapper.assistants)
                Assistant newAssistant = new Assistant(-1, true, "", "", "", "", "", new ScheduleObject(), -1, selectedInstitution.GetId());
                AddUser aa = new AddUser(newAssistant);
                if (aa.ShowDialog() == true)
                {
                    //FileTextIO.Add((Transformable)newAssistant, Assistant.PATH);
                    selectedInstitution.Users.Add(newAssistant);
                    DataObjectMapper.assistants.Add(newAssistant.GetId(), newAssistant);
                    AssistantDAL.AddAssistant(newAssistant);
                    assistants.Add(newAssistant);
                    assistantsView.Refresh();
                    MessageBox.Show("Assistant successfully added.");

                }
                else if (aa.DialogResult == false)
                {
                    aa.Close();
                }
            }
            else if (tabProfessors.IsSelected && selectedInstitution != null)
            {
                //DataObjectMapper.GenerateId(DataObjectMapper.professors)
                Professor newProfessor = new Professor(-1, true, "", "", "", "", "", new ScheduleObject(), new List<Assistant>(), selectedInstitution.GetId());
                AddUser aa = new AddUser(newProfessor);
                if (aa.ShowDialog() == true)
                {
                    //FileTextIO.Add((Transformable)newProfessor, Professor.PATH);
                    selectedInstitution.Users.Add(newProfessor);
                    DataObjectMapper.professors.Add(newProfessor.GetId(), newProfessor);
                    ProfessorDAL.AddProfessor(newProfessor);
                    professors.Add(newProfessor);
                    professorsView.Refresh();
                    MessageBox.Show("Professor successfully added.");

                }
                else if (aa.DialogResult == false)
                {
                    aa.Close();
                }
            }
            else if (tabAppointments.IsSelected && selectedInstitution != null) { }//doNothing 
          
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (dgAssistants.SelectedIndex != -1 && tabAssistants.IsSelected)
            {
                Assistant removedAssistant = (Assistant)dgAssistants.SelectedValue;
                MessageBoxResult result = (MessageBoxResult)MessageBox.Show("Do you want to remove selected assistant?", "Remove assistant", MessageBoxButtons.YesNo);
                if (MessageBoxResult.Yes == result)
                {
                    assistants.Remove(removedAssistant);
                    assistantsView.Refresh();
                    removedAssistant.Active = false;
                    //izabrani asistent se logicki brise u bazi, uklanja iz globalne mape,
                    //i ukoliko je pripadao nekom profesoru (tj. njegovo porperty Professor != -1)
                    //uklanja se iz liste asistenata pripadajuceg profesora
                   // DataObjectMapper.assistants.Remove(removedAssistant.GetId());
                    if (removedAssistant.Professor != -1) DataObjectMapper.RemoveAssistantFromProfessor(removedAssistant.Professor, removedAssistant);
                    AssistantDAL.UpdateAssistant(removedAssistant);
                    MessageBox.Show("Assistant successfully deleted.");
                }

            } else if (dgClassrooms.SelectedIndex != -1 && tabClassroom.IsSelected)
            {
                Classroom removedClassroom = (Classroom)dgClassrooms.SelectedValue;
                MessageBoxResult result = (MessageBoxResult)MessageBox.Show("Do you want to remove selected classroom?", "Remove classroom", MessageBoxButtons.YesNo);
                if (MessageBoxResult.Yes == result)
                {
                    classrooms.Remove(removedClassroom);
                    assistantsView.Refresh();
                    removedClassroom.Active = false;
                    //FileTextIO.Delete((Transformable)removedClassroom, Classroom.PATH);
                    ClassroomDAL.UpdateClassroom(removedClassroom);
                    MessageBox.Show("Classroom successfully deleted.");
                }

            } else if (dgProfessors.SelectedIndex != -1 && tabProfessors.IsSelected)
            {
                Professor removedProfessor = (Professor)dgProfessors.SelectedValue;
                MessageBoxResult result = (MessageBoxResult)MessageBox.Show("Do you want to remove selected professor?", "Remove professor", MessageBoxButtons.YesNo);
                if (MessageBoxResult.Yes == result)
                {
                    professors.Remove(removedProfessor);
                    professorsView.Refresh();
                    removedProfessor.Active = false;

                    // FileTextIO.Delete((Transformable)removedProfessor, Professor.PATH);
                    ProfessorDAL.UpdateProfessor(removedProfessor);

                    //asistenti, od obrisanog profesora, moraju postati dostupni, 
                    //tj njihov property Professor mora biti -1, i u programu i u bazi
                    removedProfessor.Assistants.ForEach(a => a.Professor = -1);
                    List<string> idAssistantsFromRemovedProfessor = removedProfessor.Assistants.Select(a => a.GetId().ToString()).ToList();
                    AssistantDAL.UpdateAssistantsForProfessor(idAssistantsFromRemovedProfessor);
                    MessageBox.Show("Professor successfully deleted.");
                }
            }
        }

        private void tbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            var searchArg = tbSearch.Text;

            if (tabClassroom.IsSelected)
            {
                classroomsView.Filter += (obj =>
                {
                    var clr = obj as Classroom;                   
                    var str = $"{clr.ClassroomNumber} {clr.Size} {clr.GetId()}";
                    return str.ToLower().Contains(searchArg);

                });
            } else if (tabAssistants.IsSelected)
            {
                assistantsView.Filter += (obj =>
                    {
                        var assistant = obj as Assistant;
                        var str = $"{assistant.GetId()}{assistant.Name}{assistant.LastName}{assistant.Email}{assistant.Password}";
                        if (assistant.Professor != -1) str.Concat(((Professor)DataObjectMapper.professors[assistant.Professor]).Name);
                        return str.ToLower().Contains(searchArg);

                    });  
            } else if (tabProfessors.IsSelected)
            {
                professorsView.Filter += (obj =>
                {
                    var professor = obj as Professor;
                    var str = $"{professor.GetId()}{professor.Name}" +
                              $"{professor.LastName}{professor.Email}" +
                              $"{professor.Password}";
                    if (professor.Assistants.Count != 0) str.Concat(String.Join("", professor.Assistants));
                    return str.ToLower().Contains(searchArg);
                });
            } else if (tabAppointments.IsSelected)
            {
                appointmentsView.Filter += (obj =>
                {
                    var appointment = obj as Appointment;
                    var str = $"{appointment.GetId()}" +
                              $"{appointment.User.Name + appointment.User.LastName }" +
                              $"{appointment.Classroom.ClassroomNumber}" +
                              $"{utility.Utility.convertDateWithTimeToString(appointment.Period.Start)}" +
                              $"{utility.Utility.convertDateWithTimeToString(appointment.Period.End)}";
                    return str.ToLower().Contains(searchArg);
                });
            }

        }
        private string getColBeingEdited(System.Windows.Controls.DataGrid dg)
        {
            var cell = dg.CurrentCell;
            var col = cell.Column;
            var x = col.Header;
            return x.ToString();
        }

        private bool isUniqeUsername(int idUser, string newUsername)
        {
            if (newUsername.Equals("")) return false;
            List<RegisteredUser> users = new List<RegisteredUser>();

            users.AddRange(DataObjectMapper.AssistantsToList());
            users.AddRange(DataObjectMapper.ProfessorsToList());
            return !users.Any(u => u.Username.Equals(newUsername) && u.GetId() != idUser);
        }

        private bool isUniqePassword(int idUser, string newPassword)
        {
            List<RegisteredUser> users = new List<RegisteredUser>();

            users.AddRange(DataObjectMapper.AssistantsToList());
            users.AddRange(DataObjectMapper.ProfessorsToList());

            return !users
             .Any(u => u.Password.Equals(newPassword) && u.GetId() != idUser);
        }

        private bool correctEmail(string newEmail)
        {
            Regex regex = new Regex(@"\b[A-Z0-9]+@[A-Z0-9]{1,}\.[A-Z]{2,3}\b", RegexOptions.IgnoreCase);
            return !newEmail.Equals(string.Empty) && regex.Match(newEmail).Success && !allSpaces(newEmail);
        }

        private bool allSpaces(string str) { return str.All(x => x.Equals(' ')); }

        public static int getIdInstitution() { return idIns; }
       
    } //od klase
}
