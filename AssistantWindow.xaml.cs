﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp
{
    /// <summary>
    /// Interaction logic for AssistantWindow.xaml
    /// </summary>
    public partial class AssistantWindow : Window
    {
        private Assistant assistant;
        private Institution institution;
        private ObservableCollection<Appointment> appointments;

        public AssistantWindow(Assistant assistant)
        {
            InitializeComponent();
            this.assistant = assistant;
            this.institution = (Institution) DataObjectMapper.institutions[assistant.Institution];

            this.appointments = new ObservableCollection<Appointment>(DataObjectMapper.AppointmentsToList()
                                                                                  .Where(appointment => appointment.Active &&
                                                                                  appointment.Classroom.Institution == institution.GetId() &&
                                                                                  appointment.User.GetId() == assistant.GetId()));

            dgAssistant.DataContext = assistant;
            dgAssistant.ItemsSource = new ObservableCollection<Assistant>() { assistant };



        }



        private void btnScheduleView_Click(object sender, RoutedEventArgs e)
        {
            if (pickerStartDate.Value < pickerEndDate.Value)
            {
                ScheduleView sc = new ScheduleView(institution, (DateTime)pickerStartDate.Value, (DateTime)pickerEndDate.Value, appointments, assistant, null);
                sc.Show();

            }
            else
            {
                MessageBox.Show("Starting date have to be greater then ending date.");
            }
        }


        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }





    }
}
