﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp
{
    /// <summary>
    /// Interaction logic for AddAssistant.xaml
    /// </summary>
    public partial class AddUser : Window
    {
        private RegisteredUser user;
        //private static int institutionId;
        public AddUser(RegisteredUser user)
        {
            InitializeComponent();
            this.user = user;
           // institutionId = insId;
            tbName.DataContext = user;
            tbLastname.DataContext = user;
            tbEmail.DataContext = user;
            tbUsername.DataContext = user;
            tbPassword.DataContext = user;
            tbInstitution.DataContext = user;

        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (user.Username.Equals("") || user.Username.All(ch => ch.Equals(' ')) || user.Password.Equals("") || user.Password.All(ch => ch.Equals(' ')))
            {
                //if (tbUsername.)
                MessageBox.Show("Username and/or password must be unique.");
            }
            else this.DialogResult = true;

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    
    }
}
