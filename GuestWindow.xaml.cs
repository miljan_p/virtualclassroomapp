﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp
{
    /// <summary>
    /// Interaction logic for GuestWindow.xaml
    /// </summary>
    public partial class GuestWindow : Window
    {
        private Guest guest;
        private ICollectionView institutionsView;
        private ICollectionView employeesView;

        private ObservableCollection<RegisteredUser> employees;
        private ObservableCollection<Institution> institutions; 

        private ObservableCollection<Appointment> appointments;

        private Institution selectedInstitution;

        public GuestWindow()
        {

           

            InitializeComponent();

            this.guest = new Guest();


            this.institutions = new ObservableCollection<Institution>(DataObjectMapper.InstitutionsToList().Where(ins => ins.Active));

            dgInstitutions.DataContext = institutions;
            institutionsView = CollectionViewSource.GetDefaultView(institutions);
            dgInstitutions.ItemsSource = institutionsView;
            dgInstitutions.IsSynchronizedWithCurrentItem = true;

        }

        private void dgInstitutions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgInstitutions.SelectedItem != null)
            {
                this.selectedInstitution = (Institution)dgInstitutions.SelectedItem;
                employees = new ObservableCollection<RegisteredUser>(selectedInstitution.Users);

                

                dgEmployees.DataContext = employees;
                employeesView = CollectionViewSource.GetDefaultView(employees);
                dgEmployees.ItemsSource = employeesView;
                dgEmployees.IsSynchronizedWithCurrentItem = true;
            }
        }


       

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnScheduleView_Click_1(object sender, RoutedEventArgs e)
        {
            if ((DateTime)pickerStartDate.Value < (DateTime)pickerEndDate.Value)
            {
                appointments = new ObservableCollection<Appointment>(DataObjectMapper.AppointmentsToList().Where(app => app.Active && ((Institution)DataObjectMapper.institutions[app.Classroom.Institution]).GetId() == selectedInstitution.GetId()));

                if (guest == null)
                {
                    MessageBox.Show("asaf");
                }
                ScheduleView sv = new ScheduleView(selectedInstitution, (DateTime)pickerStartDate.Value, (DateTime)pickerEndDate.Value, appointments, null, guest);
                sv.Show();


            }
            else
            {
                MessageBox.Show("Starting date must be lesser then ending date.");
            }
        }
    }
}
