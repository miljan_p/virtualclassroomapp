﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.DAL;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private RegisteredUser user;
        private string username;
        private string password;

        public MainWindow()
        {
            InitializeComponent();


            
           DataObjectMapper.InitialLoad();
            //MessageBox.Show(((Institution)DataObjectMapper.institutions[3]).GetId().ToString());

        }

        private void lgn(object sender, RoutedEventArgs e)
        {
            this.username = tbUsername.Text;
            this.password = pbPassword.Password;



            if (username.Equals("") || password.Equals("") || user == null)
            {
                MessageBox.Show("Make sure to provide all required data for the login.");
            } else
            {
                IScheduler<Appointment> loggedUser = (IScheduler<Appointment>)DataObjectMapper.Login(user, username, password);

                if (loggedUser != null)
                {
                    if (user is Assistant)
                    {
                     
                        Assistant a = (Assistant)loggedUser;
                        AssistantWindow aw = new AssistantWindow(a);
                        if (aw.ShowDialog() == true) { };
                       
                    }
                    else if (user is Professor)
                    {
                        Professor p = (Professor)loggedUser;
                        ProfessorWindow pw = new ProfessorWindow(p);
                        if (pw.ShowDialog() == true) { };
                   
                    }
                    else if (user is Administrator)
                    {
                        
                       AdministratorWindow aw = new AdministratorWindow(loggedUser);
                        if (aw.ShowDialog() == true)
                        {

                        };
                        //aw.Show();





                    }
                } else
                {
                    MessageBox.Show("Make sure to provide correct username and/or password.");
                }



            }

            deselect();


        }

        private void deselect()
        {
            user = null;
            username = null;
            password = null;
            btnAdministrator.IsChecked = false;
            btnAssistant.IsChecked = false;
            btnProfessor.IsChecked = false;
            tbUsername.Text = "";
            pbPassword.Password = "";
        }

        private void Professor_Checked(object sender, RoutedEventArgs e)
        {
            this.user = new Professor();
        }

        private void Assistant_Checked(object sender, RoutedEventArgs e)
        {
            this.user = new Assistant();
        }

        private void Administrator_Checked(object sender, RoutedEventArgs e)
        {
            this.user = new Administrator();
        }

        private void btnLoginAsGuest_Click(object sender, RoutedEventArgs e)
        {
            GuestWindow gw = new GuestWindow();
            if (gw.ShowDialog() == true) { };
            
        }
    }
}
