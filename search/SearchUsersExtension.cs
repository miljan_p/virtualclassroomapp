﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.search
{
    public static class SearchUsersExtension
    {
        public static IEnumerable<T> Find<T>(this IEnumerable<RegisteredUser> list, Func<T, bool> predicate)
            where T : RegisteredUser
        {
            //List<Assistant> lnew = l1.Select(assistant => (Assistant)assistant).ToList();
            //konverzija u listu u mejnu

            return list.OfType<T>().Where(predicate.Invoke);
        }
    }
}
