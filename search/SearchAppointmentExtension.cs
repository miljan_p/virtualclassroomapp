﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.search
{
    public static class SearchAppointmentExtension
    {
        public static IEnumerable<T> Find<T>(this IEnumerable<Appointment> list, Func<T, bool> predicate)
           where T : Appointment
        {


            return list.OfType<T>().Where(predicate.Invoke);
        }
    }
}
