﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.search
{
    public static class SearchClassroomExtension
    {
        public static IEnumerable<T> Find<T>(this IEnumerable<Classroom> list, Func<T, bool> predicate)
            where T : Classroom
        {


            return list.OfType<T>().Where(predicate.Invoke);
        }
    }
}
