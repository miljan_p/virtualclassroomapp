﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.search
{
    public static class SearchInstitutionExtension
    {
        public static IEnumerable<T> Find<T>(this IEnumerable<Institution> list, Func<T, bool> predicate)
           where T : Institution
        {


            return list.OfType<T>().Where(predicate.Invoke);
        }
    }
}
