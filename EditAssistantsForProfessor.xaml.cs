﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.DAL;
using VirtualClassroomApp.DAO;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp
{
    /// <summary>
    /// Interaction logic for EditAssistantsForProfessor.xaml
    /// </summary>
    public partial class EditAssistantsForProfessor : Window
    {
        private Professor professor;
        private ObservableCollection<Assistant> assistants;
        private ObservableCollection<Assistant> avaliableAssistants;

        private ICollectionView assistantsView;
        private ICollectionView avaliableAssistantsView;

        public EditAssistantsForProfessor(Professor professor)
        {
            InitializeComponent();
            this.professor = professor;

            this.assistants = new ObservableCollection<Assistant>(professor.Assistants);

            this.avaliableAssistants = new ObservableCollection<Assistant>(DataObjectMapper.AssistantsToList()
                                                                                .Where(a => a.Active &&
                                                                                a.Professor == -1 && a.Institution == professor.Institution));
            dgAssistants.DataContext = assistants;
            assistantsView = CollectionViewSource.GetDefaultView(assistants);
            dgAssistants.ItemsSource = assistantsView;
            dgAssistants.IsSynchronizedWithCurrentItem = true;

            dgAvaliableAssistants.DataContext = avaliableAssistants;
            avaliableAssistantsView = CollectionViewSource.GetDefaultView(avaliableAssistants);
            dgAvaliableAssistants.ItemsSource = avaliableAssistantsView;
            dgAvaliableAssistants.IsSynchronizedWithCurrentItem = true;

            

        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            // var currentRowIndex = dgClassrooms.Items.IndexOf(dgClassrooms.CurrentItem);
           // var currentRowIndex =  dgAssistants.Items.IndexOf(dgAssistants.CurrentItem);
            
            
            Assistant selectedAssistant = (Assistant) dgAssistants.SelectedItem;

            if (selectedAssistant != null)
            {
                selectedAssistant.Professor = -1;
                //FileTextIO.Edit((Transformable)selectedAssistant, Assistant.PATH);
                AssistantDAL.UpdateAssistant(selectedAssistant);
                this.professor.Assistants.Remove(selectedAssistant);
                assistants.Remove(selectedAssistant);          
                if(selectedAssistant.Active)avaliableAssistants.Add(selectedAssistant);
                MessageBox.Show("Selected assistant is removed from list of assistants.");
            }


        }

        private void btnSaveChanges_Click(object sender, RoutedEventArgs e)
        {
            
            this.Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Assistant selectedAvaliableAssistant = (Assistant)dgAvaliableAssistants.SelectedItem;

            if (selectedAvaliableAssistant != null)
            {
                selectedAvaliableAssistant.Professor = this.professor.GetId();
                this.professor.Assistants.Add(selectedAvaliableAssistant);
                // FileTextIO.Edit((Transformable)selectedAvaliableAssistant, Assistant.PATH);
                AssistantDAL.UpdateAssistant(selectedAvaliableAssistant);
                avaliableAssistants.Remove(selectedAvaliableAssistant);
                assistants.Add(selectedAvaliableAssistant);
                MessageBox.Show("Selected assistant is successfuly added to list of assistant of this professor.");
            }
        }
    }
}
