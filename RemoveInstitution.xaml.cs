﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.DAL;
using VirtualClassroomApp.DAO;
using VirtualClassroomApp.model;
using MessageBox = System.Windows.Forms.MessageBox;

namespace VirtualClassroomApp
{
    /// <summary>
    /// Interaction logic for RemoveInstitution.xaml
    /// </summary>
    public partial class RemoveInstitution : Window
    {
        private Institution selectedInstitution;
        
        public RemoveInstitution(Institution selectedInstitution)
        {
            InitializeComponent();
            this.selectedInstitution = selectedInstitution;
            this.DataContext = this.selectedInstitution;

            dgClassrooms.ItemsSource = DataObjectMapper.ClassroomsToList()
                                                 .FindAll(classroom => classroom.Active &&
                                                                       classroom.Institution == 
                                                                       selectedInstitution.GetId());
       
            dgUsers.ItemsSource = DataObjectMapper.UsersToList()
                                            .FindAll(user => user.Active && 
                                                     (selectedInstitution.GetId() == 
                                                            getInstitutionForUser(user)));

        }

        private int getInstitutionForUser(RegisteredUser user)
        {
            if (user is Assistant)
            {
                Assistant assistant = (Assistant)user;
                return assistant.Institution;
            }
            Professor professor = (Professor)user;
            return professor.Institution;
              
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            //public static DialogResult Show(string text, string caption, MessageBoxButtons buttons);
            MessageBoxResult result = (MessageBoxResult) MessageBox.Show("Do you want to remove this institution", "Remove institution", MessageBoxButtons.YesNo);

            if (MessageBoxResult.Yes == result)
            {
                selectedInstitution.Active = false;
                // FileTextIO.Delete(selectedInstitution, Institution.PATH);
                InstitutionDAL.UpdateInstitution(selectedInstitution);
                MessageBox.Show("You have successfully removed " + " " + selectedInstitution.Name + ".");
                this.DialogResult = true;
                this.Close();
            } 

        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
