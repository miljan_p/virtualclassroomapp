﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace VirtualClassroomApp.Validations
{
    public class ClassroomSizeValidation : ValidationRule
    {

        

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int size;
            if (int.TryParse(value.ToString(), out size))
                return size < 0 ? new ValidationResult(false, "Classroom size must be greater then 0.") : new ValidationResult(true, null);
            else return new ValidationResult(false, "Classroom size must be a number greater then 0.");
        }
    }
}
