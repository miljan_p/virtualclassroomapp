﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace VirtualClassroomApp.Validations
{
    class EmailValidationRule   : ValidationRule
    {
        Regex regex = new Regex(@"\b[A-Z0-9]+@[A-Z0-9]{1,}\.[A-Z]{2,3}\b", RegexOptions.IgnoreCase);

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string v = value as string;
            if (v != null && !v.Equals(string.Empty) && regex.Match(v).Success)
                return new ValidationResult(true, null);
            if (v.Equals(string.Empty))
                return new ValidationResult(false, "This field is required");
            //if (value.ToString().Equals("") || value.ToString().Any(x => value.Equals(" "))) return new ValidationResult(false, "Cannot be empty string or spaces string only");
            return new ValidationResult(false, "Wrong format!");
        }
    }
    
}
