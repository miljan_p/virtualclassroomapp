﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.Validations
{
    public class PasswordValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var newPassword = (string)value;
            List<RegisteredUser> users = new List<RegisteredUser>();
            int idIns = AdministratorWindow.getIdInstitution();
            users.AddRange(DataObjectMapper.AssistantsToList().Where(a => a.Institution == idIns));
            users.AddRange(DataObjectMapper.ProfessorsToList().Where(p => p.Institution == idIns));
            bool taken = false;

            foreach (RegisteredUser user in users)
            {
                if (user.Password.Equals(newPassword)) { taken = true; break; };

            }
            if (taken)
            {
                return new ValidationResult(false, "asfasf");
            }
            else if (newPassword.ToString().Equals("") || !newPassword.ToString().Any(x => char.IsLetterOrDigit(x)))
            {
                return new ValidationResult(false, "asfasf");
            }
            else
            {
                return new ValidationResult(true, "tsgd");
            }

        }
    }
}
