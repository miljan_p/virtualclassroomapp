﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace VirtualClassroomApp.Validations
{
    public class AssistantNameValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            
            int nameAsNumber;
            if (int.TryParse(value.ToString(), out nameAsNumber))
            {
                return new ValidationResult(false, "Cannot be number");
            } 
            if (value.ToString().Equals("") || value.ToString().Any(x => x.Equals(' '))) return new ValidationResult(false, "Cannot be empty string or spaces string only");
            return new ValidationResult(true, "tsgd");

                
        }
    }
}
