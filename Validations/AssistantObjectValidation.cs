﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.Validations
{
    class AssistantObjectValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var assistant = value as Assistant;

            var nameValidator = new AssistantNameValidation();

            if (!nameValidator.Validate(assistant.Name, null).IsValid) return new ValidationResult(false, null);




            else return new ValidationResult(true, null);
           
        }
    }
}
