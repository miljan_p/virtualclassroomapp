﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.Validations
{
    public class ClassroomObjectValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {  

            var classroom = value as Classroom;
            var oldClassroomNumber = classroom.ClassroomNumber;
            //int IdIns = AdministratorWindow.getIdInstitution();

            if (!ClassroomNumbersAreUnique(classroom)) { classroom.ClassroomNumber = oldClassroomNumber; new ValidationResult(false, null); }
                
            //if ()


            //var ClassroomSizeRule = new ClassroomSizeValidation();
            //var ClassroomNumberRule = new ClassroomNumberValidation();
            //var ClassroomUniqueNumberValidation = new ClassroomUniqueNumberValidation();

            //if (!ClassroomSizeRule.Validate(classroom.Size, null).IsValid) return ClassroomSizeRule.Validate(classroom.Size, null);
            //else if (!ClassroomNumberRule.Validate(classroom.ClassroomNumber, null).IsValid) return ClassroomNumberRule.Validate(classroom.ClassroomNumber, null);
            ////else if (!ClassroomUniqueNumberValidation.Validate(classroom, null).IsValid) return ClassroomUniqueNumberValidation.Validate(classroom, null);


            //if (DataMapper.ClassroomNumberExsist(classroom))
            //{
            //    classroom.ClassroomNumber = DataMapper.generateRoomNumber(classroom.Institution);
            //    return new ValidationResult(false, "asfafa");
            //}
            // else
            return new ValidationResult(true, null);
        
        }

        private bool ClassroomNumbersAreUnique(Classroom classroom)
        { //var anyDuplicate = enumerable.GroupBy(x => x.Key).Any(g => g.Count() > 1);
            int idIns = classroom.Institution;

            return DataObjectMapper
                .ClassroomsToList()
                .Where(clr => clr.Active && clr.Institution == idIns && clr.GetId() != classroom.GetId())
                .GroupBy(clr => clr.ClassroomNumber).Any(group => group.Count() > 1);
        }


    }
}
