﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.Validations
{
    public class ClassroomNumberValidation : ValidationRule
    {
        private int ins;
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            //this.ins = AddClassroom.idInstituion();
            this.ins = AdministratorWindow.getIdInstitution();
            int broj;
            if (int.TryParse(value.ToString(), out broj))
                if (broj < 0) return new ValidationResult(false, "Mora veci od 0");
                //return  ? new ValidationResult(false, "Mora veci od 0") : new ValidationResult(true, null);
            if (string.IsNullOrEmpty(((string)value))){ return new ValidationResult(false, "ne sme biti prazan"); }
            int newClassroomNumber = int.MaxValue;
            try
            {
                newClassroomNumber =  int.Parse((string)value);


            } catch (Exception)
            {
                return new ValidationResult(false, "Mora veci od 0");
            }

            //bool res = ((Institution)DataObjectMapper.institutions[ins]).Classrooms.Any(clr => clr.ClassroomNumber == br && clr.GetId() != );
            //bool res = ClassroomNumbersAreUnique(newClassroomNumber);
            //return res  ? new ValidationResult(true, "") : new ValidationResult(false, "");
            return new ValidationResult(true, "");


        }

        private bool ClassroomNumbersAreUnique(int newClassroomNumber)
        { //var anyDuplicate = enumerable.GroupBy(x => x.Key).Any(g => g.Count() > 1);
            //int idIns = classroom.Institution;
            return ((Institution)DataObjectMapper.institutions[ins]).Classrooms
                .Where(clr => clr.Active && clr.Institution == ins)
                .Any(clr => clr.ClassroomNumber == newClassroomNumber);

        }
    }
}
