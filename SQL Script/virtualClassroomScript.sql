use [Virtual Classroom]

create table RegisteredUser
(
	  Id int Primary Key identity(1, 1), 
	  Active bit not null,
	  FirstName varchar(30) Not Null,
	  LastName varchar(30) Not null,
	  Email varchar(30) not null,
	  Username varchar(30) not null,
	  Passwordd varchar(30) not null,
	  UserType char(2) not null 
)
Insert Into RegisteredUser Values
	( 1, 'Administrator', 'Administratorovic', 'admin@uns.ac.rs', 'admin', '010', 'Aa');
Insert Into RegisteredUser Values
	( 1, 'Aleksandar', 'Peric', 'aleksandar@uns.ac.rs', 'Aca', '121', 'Pr');
Insert Into RegisteredUser Values
	( 1, 'Petar', 'Petrovic', 'petar@uns.ac.rs', 'Pera', '122', 'As'); --on je aleksandrov assistent
Insert Into RegisteredUser Values
	( 1, 'Djurdje', 'Djuric', 'djura@uns.ac.rs', 'Djura', '123', 'As'); --niciji assistent
Insert Into RegisteredUser Values
	( 1, 'Marko', 'Maric', 'marko@uns.ac.rs', 'Mare', '124', 'As'); --niciji assistent 

Insert Into RegisteredUser Values
	( 1, 'Profesor', 'Profic', 'prof@uns.ac.rs', 'Profa', '000', 'Pr'); --niciji prof 


select * from RegisteredUser

--upit za proveru dostupnosti lozinke, a moze i za korisnicko ime
select distinct(Passwordd)
from RegisteredUser
left join Assistant on RegisteredUser.Id = Assistant.Id
left join Professor on Professor.Id = Professor.Id
where Assistant.Institution = 1 AND Professor.Institution = 1 and RegisteredUser.Passwordd = '123'

create table Professor
	(
		Id int primary key,
		Institution int not null,
		foreign key (Institution) references Institution(Id),
		foreign key (Id) references RegisteredUser(Id) 
	)
Insert Into Professor Values
	(2, 1); --Aleksandar Peric profesor (ima 3 ass sa idijevaim 3,4,5 Petar,Djurdje, Marko)
Insert Into Professor Values
	(6, 1); --niciji prof profa, nema assistente
select * from Professor

--upit za ucitavanje svih profesora
select r.Id, r.Active, r.FirstName, r.LastName, r.Email, p.Institution, r.Username, r.Passwordd 
from RegisteredUser r, Professor p 
where r.Id = p.Id


create table Assistant
(
     Id int Primary Key NOT NULL  ,
	 Professor int DEFAULT null,
	 Institution int not null,
	 foreign key (Institution) references Institution(Id),
	 foreign key (Professor) references RegisteredUser(Id),
	 foreign key (Id) references RegisteredUser(Id)
	
)
Insert Into Assistant Values
	(3, 2, 1); --za assistenta Petra, profesor mu je Aleksandar
Insert Into Assistant Values
	(4, 2, 1); --za assistenta Djurdje, on je niciji assistent
Insert Into Assistant Values
	(5, 2, 1); --za assistenta Marko, on je niciji assistent

select * from Assistant

--ucitavanje svih asistenata
select r.Id, r.Active, r.FirstName, r.LastName, r.Email, r.Username, r.Passwordd, a.Professor, a.Institution
from RegisteredUser r, Assistant a
where r.Id = a.Id 







create table Institution 
(
	Id int primary key  not null,
	Active bit not null,
	NameInstitution varchar(30) not null,
	Addres varchar(50) not null,
)
Insert Into Institution Values
	(1,  1, 'Fakultet Tehnickih Nauka', 'Trg Dositeja Obradovica 6');
Insert Into Institution Values
	(2, 1, 'Tehnoloski fakultet', 'Bulevar Cara Lazara 1');
Insert Into Institution Values
	(3, 1, 'Pravni fakultet', 'Bulevar Zorana Djindjica 3');
select * from Institution



create table Classroom
(
	Id int primary key not null,
	Active bit not null,
	ClassroomNumber int not null,
	Size int null,
	Computers bit not null,
	Institution int not null,
	foreign key (Institution) references Institution(Id)
)
Insert Into Classroom Values
	(1, 1, 1, 20, 1, 1);
Insert Into Classroom Values
	(2, 1, 2, 25, 1, 1);
select * from Classroom

--upita za proveravanje jedinsvetnosti broja ucionice (property: ClassroomNumber)
select * 
from Classroom
where Id != 1 and Institution = 1 and ClassroomNumber = 3

create table Period
(
 Id int primary key,
 Active bit not null,
 StartDate datetime not null,
 EndDate datetime not null,
)


insert into Period Values
	(1, 1, '10-02-2020 13:00', '12-02-2020 13:00');
insert into Period Values
	(2, 1, '11-02-2020 13:00', '15-02-2020 13:00');
insert into Period Values
	(3, 1, '18-02-2020 13:00', '20-02-2020 13:00');
delete from Period
select * from Period


create table Appointment
(
	Id int primary key not null,
	Active bit not null,
	PeriodId int not null,
	ClassroomId int not null,
	TeachingForm int not null,
	UserId int not null,
	foreign key (PeriodId) references Period(Id),
	foreign key (ClassroomId) references Classroom(Id),
	foreign key (UserId) references RegisteredUser(Id)
)

insert into Appointment Values
	(1, 1, 1, 1, 1, 3);
insert into Appointment Values
	(2, 1, 2, 2, 1, 4);
insert into Appointment Values
	(3, 1, 3, 2, 1, 5);
delete from Appointment

select * from Appointment
select * from Period





drop table Appointment
DROP TABLE Classroom
drop table Assistant
drop table Professor
DROP TAble RegisteredUser
drop table Institution
drop table Period



--ALL AKTIV PROF PROFESSORS FOR SOME INSTI

--select r.Id, r.Active, r.FirstName, r.LastName, r.Email, r.Username, r.Passwordd, p.Institution
--from RegisteredUser r, Professor p
--where r.Id = p.iD and r.Active = 1 and p.Institution = 

