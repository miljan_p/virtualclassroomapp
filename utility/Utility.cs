﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace VirtualClassroomApp.utility
{
    public class Utility
    {

        public static DateTime createDateWithTime(string datum)
        {
            DateTime dateValue;

            if (DateTime.TryParseExact(datum, "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateValue))
            {
                return dateValue;
            }
            else
            {
                throw new FormatException("Datum nije uspešno konvertovan prilikom citanja iz fajla");
            }


        }

        public static string convertDateWithTimeToString(DateTime date)
        {
            
            return date.ToString("dd-MM-yyyy HH:mm");
        }












    }
}
