﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using VirtualClassroomApp.DAO;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.controller
{
    public class DataMapper
    {

        public static Dictionary<int, Transformable> assistants = null;
        public static Dictionary<int, Transformable> professors = null;
        public static Dictionary<int, Transformable> administrators = null;
        public static Dictionary<int, Transformable> classrooms = null;
        public static Dictionary<int, Transformable> institutions = null;
        public static Dictionary<int, Transformable> periods = null;
        public static Dictionary<int, Transformable> appointments = null;

        public static void InitialLoad()
        {
            assistants = FileTextIO.Load(new Assistant(), Assistant.PATH);
            professors = FileTextIO.Load(new Professor(), Professor.PATH);
            administrators = FileTextIO.Load(new Administrator(), Administrator.PATH);
            classrooms = FileTextIO.Load(new Classroom(), Classroom.PATH);
            institutions = FileTextIO.Load(new Institution(), Institution.PATH);
            periods = FileTextIO.Load(new Period(), Period.PATH);
            appointments = FileTextIO.Load(new Appointment(), Appointment.PATH);

        }

        public static List<RegisteredUser> FindUsersForInstitution(int idInstitution)
        {

            List<RegisteredUser> usersForInstitution = new List<RegisteredUser>();

            assistants.Values.ToList()
           .ConvertAll(assistant => (Assistant)assistant)
           .Where(assistant => assistant.Institution == idInstitution)
           .ToList().ForEach(assistant => usersForInstitution.Add(assistant));

            professors.Values.ToList()
           .ConvertAll(professor => (Professor)professor)
           .Where(professor => professor.Institution == idInstitution)
           .ToList().ForEach(professor => usersForInstitution.Add(professor));

            return usersForInstitution;
        }

        public static List<Classroom> FindClassroomsForInstitution(int idInstitution)
        {
            return classrooms.Values.ToList()
                .ConvertAll(classroom => (Classroom)classroom)
                .Where(classroom => classroom.Institution == idInstitution)
                .ToList();

        }



        public static List<Assistant> FindAssistantsForProfessor(int idProfessor)
        {
            return assistants.Values.ToList()
                .ConvertAll(assistant => (Assistant)assistant)
                .Where(assistant => assistant.Professor == idProfessor)
                .ToList();
        }

        public static int GenerateId(Dictionary<int, Transformable> mapa)
        {
            if (!(mapa.Count() == 0)) return mapa.Keys.Max() + 1;
            return 0;
        }

        public static int generateRoomNumber(int idInstitution)
        {
            if (!(classrooms.Count == 0))
            {
                return classrooms.Values.ToList()
                      .ConvertAll(classroom => (Classroom)classroom)
                      .Where(classroom => classroom.Institution == idInstitution)
                      .ToList().Select(x => x.ClassroomNumber).Max() + 1;
            }
            else return 1;
        }

        public static bool ClassroomNumberExsist(Classroom clr)
        {
            if (!(classrooms.Count == 0))
            {
                try
                {
                    return classrooms.Values.ToList()
                        .ConvertAll(classroom => (Classroom)classroom).ToList()
                        .Any(c => c.Institution == clr.Institution && c.ClassroomNumber == clr.ClassroomNumber && c.GetId() != clr.GetId());
                }
                catch (Exception)
                {
                    return false;
                }


            }
            else return false;



        }



        public static bool avaliable(Classroom classroom, Period period)
        {
            int idIns = classroom.Institution;

            List<Appointment> appointmentsForClassroom =
                appointments.Values.ToList()
                .ConvertAll(appointment => (Appointment)appointment)
                .Where(appointment => //appointment.Classroom.Institution == idIns &&
                        classroom.GetId() == appointment.Classroom.GetId() && 
                        appointment.Active).ToList();


            if (appointmentsForClassroom.Count == 0) return true;

            int overlapedAppointmentsForClassroom = (int)appointmentsForClassroom
                .Where(appointment => appointment.Period.Overlaps(period))
                .Count();

            return overlapedAppointmentsForClassroom == 0 ? true : false;
        }

        public static bool avaliableWhileEditing(Classroom classroom, Period period, int idAppointment, int userId)
        {
            
            int idIns = classroom.Institution;

            List<Appointment> appointmentsForClassroom =
                appointments.Values.ToList()
                .ConvertAll(appointment => (Appointment)appointment)
                .Where(appointment => //appointment.Classroom.Institution == idIns &&
                        classroom.GetId() == appointment.Classroom.GetId() &&
                        appointment.Active &&
                        appointment.GetId() != idAppointment)
                .ToList();

            //if (appointmentsForClassroom.Count != 0) return false;

            
            int overlapedAppointmentsForClassroom = (int)appointmentsForClassroom
                .Where(appointment => appointment.Period.Overlaps(period))
                .Count();

            if (overlapedAppointmentsForClassroom != 0) return false;


            List <Appointment> appointmentsForUser = appointments.Values.ToList()
                .ConvertAll(appointment => (Appointment)appointment)
                .Where(appointment => appointment.Active && appointment.GetId() != idAppointment && appointment.User.GetId() == userId)
                .ToList();

            int overlapedAppointmentsForUser = (int)appointmentsForUser
                .Where(appointment => appointment.Period.Overlaps(period))
                .Count();

            return overlapedAppointmentsForUser == 0 ? true : false;
        }


        public static Transformable Login(Transformable userType, String username, String password)
        {
            Transformable user = null;

            if (userType is Assistant)
            {
                try
                {
                    user = assistants.Values.ToList()
                    .ConvertAll(assistant => (Assistant)assistant)
                    .Where(assistant => assistant.Username.Equals(username) &&
                                        assistant.Password.Equals(password)).Single();
                }
                catch (InvalidOperationException e)
                {
                    return user = null;
                }
                catch (ArgumentNullException e)
                {
                    return user = null;
                }

            }
            else if (userType is Professor)
            {

                try
                {
                    user = professors.Values.ToList()
                   .ConvertAll(professor => (Professor)professor)
                   .Where(professor => professor.Username.Equals(username) &&
                                       professor.Password.Equals(password)).Single();

                }
                catch (InvalidOperationException e)
                {
                    return user = null;
                }
                catch (ArgumentNullException e)
                {
                    return user = null;
                }

            }
            else if (userType is Administrator)
            {

                try
                {
                    user = administrators.Values.ToList()
                     .ConvertAll(administrator => (Administrator)administrator)
                     .Where(administrator => administrator.Username.Equals(username) &&
                                         administrator.Password.Equals(password)).Single();


                }
                catch (InvalidOperationException e)
                {
                    return user = null;
                }
                catch (ArgumentNullException e)
                {
                    return user = null;
                }

            }

            return user;
        }


        public static List<Institution> InstitutionsToList()
        {
            return institutions.Values.ToList()
           .ConvertAll(institution => (Institution)institution)
           .ToList();

        }

        public static List<Classroom> ClassroomsToList()
        {
            return classrooms.Values.ToList()
           .ConvertAll(classroom => (Classroom)classroom)
           .ToList();

        }


        public static List<RegisteredUser> UsersToList()
        {
            List<RegisteredUser> registerdUsers = new List<RegisteredUser>();

            assistants.Values.ToList()
           .ConvertAll(assistant => (Assistant)assistant)   
           .ToList().ForEach(assistant => registerdUsers.Add(assistant));

            professors.Values.ToList()
           .ConvertAll(professor => (Professor)professor)
           .ToList().ForEach(professor => registerdUsers.Add(professor));

            return registerdUsers;
        }


        public static List<Assistant> AssistantsToList()
        {
           return assistants.Values.ToList()
          .ConvertAll(assistant => (Assistant)assistant)
          .ToList();
        }

        public static List<Professor> ProfessorsToList()
        {
            return professors.Values.ToList()
           .ConvertAll(professor => (Professor)professor)
           .ToList();
        }

        public static  List<Appointment> AppointmentsToList() { 
        
            return appointments.Values.ToList()
           .ConvertAll(appointment => (Appointment)appointment)
           .ToList();
        }


    



    }
}
