﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.model;
using MessageBox = System.Windows.Forms.MessageBox;

namespace VirtualClassroomApp
{
    /// <summary>
    /// Interaction logic for AddAppointmentAdmin.xaml
    /// </summary>
    public partial class AddAppointmentAdmin : Window
    {

        private Classroom classroom;
        private Period period;
        private RegisteredUser user;
        private Appointment newApointment = new Appointment(); //DataObjectMapper.GenerateId(DataObjectMapper.appointments)
        private RegisteredUser ru;

        private int idInstitution;
        private ObservableCollection<Classroom> allClassrooms;
        private ObservableCollection<Professor> allProfessors;
        private ObservableCollection<Assistant> allAssistants;
        private ObservableCollection<Appointment> appointments;

        public AddAppointmentAdmin(int idInstitution, ObservableCollection<Appointment> appointments, RegisteredUser ru)
        {
            InitializeComponent();
            
            this.idInstitution = idInstitution;
            this.appointments = appointments;
           //newApointment.SetId(DataObjectMapper.GenerateId(DataObjectMapper.appointments));
            newApointment.Period = new Period();
            //newApointment.Period.SetId(DataObjectMapper.GenerateId(DataObjectMapper.periods));
            newApointment.Period.Active = true;

           this.allClassrooms = getClassrooms();
            
            //else this.allClassrooms = getClassroomsForRegisteredUser();
            
            this.allProfessors = getProfessors();
            this.allAssistants = getAssistants();


            pickerStartDate.DataContext = newApointment.Period;
            pickerEndDate.DataContext = newApointment.Period;


            dgClassrooms.DataContext = newApointment;
            dgClassrooms.ItemsSource = allClassrooms;

            dgAssistants.DataContext = newApointment;
            dgAssistants.ItemsSource = allAssistants;

            dgProfessors.DataContext = newApointment;
            dgProfessors.ItemsSource = allProfessors;

            if (ru != null)
            {
                newApointment.User = ru;
                lblProfessors.Visibility = Visibility.Hidden;
                lblAssistants.Visibility = Visibility.Hidden;
                
                dgAssistants.Visibility =  Visibility.Hidden;
                dgProfessors.Visibility = Visibility.Hidden;

            }


        }

     


        private ObservableCollection<Classroom> getClassrooms()
        {
            return new ObservableCollection<Classroom>(DataObjectMapper.ClassroomsToList().FindAll(cl => cl.Active && cl.Institution == idInstitution));

        }

        private ObservableCollection<Professor> getProfessors()
        {
            return new ObservableCollection<Professor>(DataObjectMapper.ProfessorsToList().FindAll(pr => pr.Active && pr.Institution == idInstitution));
        }

        private ObservableCollection<Assistant> getAssistants()
        {
            return new ObservableCollection<Assistant>(DataObjectMapper.AssistantsToList().FindAll(assistant => assistant.Active && assistant.Institution == idInstitution));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (newApointment.Period.Start < newApointment.Period.End)
            {
                // Appointment zakazani = (Appointment)(DataObjectMapper.appointments[DataObjectMapper.appointments.Keys.Max()]);
                MessageBox.Show("new id od appointment: " + newApointment.GetId().ToString() + "\n" + "zakazivac " + newApointment.User.Name + "\n" + "start: " + newApointment.Period.Start.ToString() + "\n" + "END: " + newApointment.Period.End.ToString() + "\n" + "CLR NUMBER: " + newApointment.Classroom.ClassroomNumber.ToString());



                if (newApointment.ScheduledBy((IScheduler<Appointment>)newApointment.User))
                {

                    //Appointment zakazani = (Appointment)(DataObjectMapper.appointments[DataObjectMapper.appointments.Keys.Max()]);
                    //MessageBox.Show("new id od appointment: " + zakazani.GetId().ToString() + "\n" + "zakazivac " + zakazani.User.Name + "\n" + "start: " + zakazani.Period.Start.ToString() + "\n" + "END: " + zakazani.Period.End.ToString() + "\n");

                    appointments.Add((Appointment)DataObjectMapper.appointments[DataObjectMapper.appointments.Keys.Max()]);




                    MessageBox.Show("Appointment added.");

                    this.Close();

                }
                else
                {
                    MessageBox.Show("Appointment cannot be added. It overlaps with some other appointments or the user have already booked appointment at that time.");
                }
            }
            else {MessageBox.Show("End date must be larger then start date."); }
        }

        private void btnCheck_Click(object sender, RoutedEventArgs e)
        {
            


            if (this.newApointment.User != null)
            {
                if (newApointment.Period.Start != null)
                {
                    if (newApointment.Period.End != null)
                    {
                        if (newApointment.Classroom != null)
                        {

                            var user = this.newApointment.User.Name;
                            var start = newApointment.Period.Start.ToString();
                            var end = newApointment.Period.End.ToString();
                            var classroom = newApointment.Classroom.ClassroomNumber.ToString();
                            MessageBox.Show("user: " + user + "\n" + "start: "  + start + "\n" + "end: " + end + "\n" + "classroom number: " + classroom);


                        }
                        else
                        {
                            MessageBox.Show("Classroom je null");
                        }

                    }
                    else
                    {
                        MessageBox.Show("End je null");
                    }

                }
                else
                {
                    MessageBox.Show("Start je null");
                }

            }
            else
            {
                MessageBox.Show("User je null");
            }

        }


        private void dgAssistants_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dgProfessors.SelectedIndex = -1;
        }

        private void dgProfessors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dgAssistants.SelectedIndex = -1;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
