﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualClassroomApp.model;
using VirtualClassroomApp.utility;

namespace VirtualClassroomApp.DAL
{
    public class PeriodDAL
    {

        private const string ConnectionString = "Data Source=DESKTOP-3OB8NQ2\\SQLEXPRESS;" +
         "                                     Integrated Security = True; Connect Timeout = 30; " +
         "                                     Encrypt=False;TrustServerCertificate=False;" +
         "                                     ApplicationIntent=ReadWrite;MultiSubnetFailover=False;Initial Catalog=Virtual Classroom;";

        public static Dictionary<int, Transformable> LoadAllPeriods()
        {
            //from all institutions, (non)active...

            Dictionary<int, Transformable> periods = new Dictionary<int, Transformable>();


            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = connection.CreateCommand();
                var dataAdapter = new SqlDataAdapter();
                command.CommandText = "SELECT Id, Active, StartDate, EndDate FROM Period";
                dataAdapter.SelectCommand = command;
                var dataSet = new DataSet();

                dataAdapter.Fill(dataSet, "Period");

                foreach (DataRow dataRow in dataSet.Tables["Period"].Rows)
                {
                    var period = new Period();
                    period.SetId((int)dataRow["Id"]);
                    period.Active = (bool)dataRow["Active"];
                    period.Start = Utility.createDateWithTime((string)dataRow["StartDate"]);//;
                    period.End = Utility.createDateWithTime((string)dataRow["EndDate"]); ;


                    periods.Add(period.GetId(), (Transformable)period);

                }
            }
            return periods;
        }


        public static void UpdatePeriod(Period period)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand dataCommand = new SqlCommand();
                    dataCommand.Connection = connection;

                    dataCommand.CommandText = @"INSERT INTO Period "
                        + "(Id, Active, PeriodStart, PeriodEnd) VALUES (@Id, @Active, @StartDate, @EndDate)";

                    SqlParameter paramId = new SqlParameter("@Id", period.GetId());
                    SqlParameter paramActive = new SqlParameter("@Active", Convert.ToInt32(period.Active));
                    SqlParameter paramStartDate = new SqlParameter("@StartDate", period.Start);
                    SqlParameter paramEndDate = new SqlParameter(@"EndDate", period.End);

                    dataCommand.Parameters.AddRange(new SqlParameter[] { paramId, paramActive, paramStartDate, paramEndDate });
                    dataCommand.ExecuteNonQuery();


                }
                catch (Exception)
                {

                }


            }
        }

        public static void AddPeriod(Period period)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand dataCommand = new SqlCommand();
                    dataCommand.Connection = connection;

                    dataCommand.CommandText = @"UPDATE Period SET Active=@Active, StartDate=@StartDate, EndDate=@EndDate WHERE Id=@Id";

                    SqlParameter paramId = new SqlParameter("@Id", period.GetId());
                    SqlParameter paramActive = new SqlParameter("@Active", Convert.ToInt32(period.Active));
                    SqlParameter paramStartDate = new SqlParameter("@StartDate", period.Start);
                    SqlParameter paramEndDate = new SqlParameter(@"EndDate", period.End);

                    dataCommand.Parameters.AddRange(new SqlParameter[] { paramId, paramActive, paramStartDate, paramEndDate });
                    dataCommand.ExecuteNonQuery();


                }
                catch (Exception)
                {

                }


            }
        }


    }
}
