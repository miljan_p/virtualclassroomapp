﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.DAL
{
    public class AdministratorDAL
    {

        private const string ConnectionString = "Data Source=DESKTOP-3OB8NQ2\\SQLEXPRESS;" +
          "                                     Integrated Security = True; Connect Timeout = 30; " +
          "                                     Encrypt=False;TrustServerCertificate=False;" +
          "                                     ApplicationIntent=ReadWrite;MultiSubnetFailover=False;Initial Catalog=Virtual Classroom;";

        public static Dictionary<int, Transformable> LoadAllAdministrators()
        {
            //from all institutions, (non)active...

            Dictionary<int, Transformable> administrators = new Dictionary<int, Transformable>();


            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = connection.CreateCommand();
                var dataAdapter = new SqlDataAdapter();
                command.CommandText = "select Id, Active, FirstName, LastName, Email, Username, Passwordd FROM RegisteredUser WHERE UserType=@UserType";
                SqlParameter paramUserType = new SqlParameter("@UserType", "Aa");
                command.Parameters.Add(paramUserType);
                dataAdapter.SelectCommand = command;
                var dataSet = new DataSet();

                dataAdapter.Fill(dataSet, "Administrators");

                foreach (DataRow dataRow in dataSet.Tables["Administrators"].Rows)
                {
                    var administrator = new Administrator();
                    administrator.SetId((int)dataRow["Id"]);
                    administrator.Active = (bool)dataRow["Active"];
                    administrator.Name = (string)dataRow["FirstName"];
                    administrator.LastName = (string)dataRow["LastName"];
                    administrator.Email = (string)dataRow["Email"];
                    administrator.Username = (string)dataRow["Username"];
                    administrator.Password = (string)dataRow["Passwordd"];

                    administrators.Add(administrator.GetId(), (Transformable)administrator);

                }
            }

            return administrators;

        } // od metode LoadAllAdministrators()
    }
}
