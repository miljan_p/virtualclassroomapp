﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.DAL
{
    public class InstitutionDAL
    {

        private const string ConnectionString = "Data Source=DESKTOP-3OB8NQ2\\SQLEXPRESS;" +
           "                                     Integrated Security = True; Connect Timeout = 30; " +
           "                                     Encrypt=False;TrustServerCertificate=False;" +
           "                                     ApplicationIntent=ReadWrite;MultiSubnetFailover=False;Initial Catalog=Virtual Classroom;";
        public static  Dictionary<int, Transformable> LoadAllInstitutions()
        {
            Dictionary<int, Transformable> institutions = new Dictionary<int, Transformable>();


            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = connection.CreateCommand();
                var dataAdapter = new SqlDataAdapter();
                command.CommandText = "SELECT Id, Active, NameInstitution, Addres FROM Institution";
                dataAdapter.SelectCommand = command;
                var dataSet = new DataSet();

                dataAdapter.Fill(dataSet, "Institution");

                foreach (DataRow dataRow in dataSet.Tables["Institution"].Rows)
                {
                    var institution = new Institution();
                    institution.SetId((int)dataRow["Id"]);
                    institution.Active = (bool)dataRow["Active"];
                    institution.Name = (string)dataRow["NameInstitution"];
                    institution.Address = (string)dataRow["Addres"];
                    institution.Classrooms = DataObjectMapper.FindClassroomsForInstitution(institution.GetId());
                    institution.Users = DataObjectMapper.FindUsersForInstitution(institution.GetId());



                    institutions.Add(institution.GetId(), (Transformable)institution);

                }
            }

            return institutions;
        }




        public static void AddInstitution(Institution institution)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand dataCommand = new SqlCommand();
                    dataCommand.Connection = connection;
                   
                    dataCommand.CommandText = @"INSERT INTO Institution "
                        + "(Id, Active, NameInstitution, Addres) VALUES (@Id, @Active, @NameInstitution, @Addres)"; // +   Active=@Active, NameInstitution=@NameInstitution, Addres = @Addres WHERE Id=@Id";

                    SqlParameter paramId = new SqlParameter("@Id", institution.GetId());
                    SqlParameter paramActive = new SqlParameter("@Active", Convert.ToInt32(institution.Active));
                    SqlParameter paramNameInstitution = new SqlParameter("@NameInstitution", institution.Name);
                    SqlParameter paramAddres = new SqlParameter(@"Addres", institution.Address);


                    dataCommand.Parameters.AddRange(new SqlParameter[] { paramId, paramActive, paramNameInstitution, paramAddres });
                    dataCommand.ExecuteNonQuery();


                }
                catch (Exception)
                {

                }
            }
        }

        public static void UpdateInstitution(Institution institution)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand dataCommand = new SqlCommand();
                    dataCommand.Connection = connection;

                    dataCommand.CommandText = @"UPDATE Institution SET Active=@Active, NameInstitution=@NameInstitution, Addres = @Addres WHERE Id=@Id";

                    SqlParameter paramId = new SqlParameter("@Id", institution.GetId());
                    SqlParameter paramActive = new SqlParameter("@Active", Convert.ToInt32(institution.Active));
                    SqlParameter paramNameInstitution = new SqlParameter("@NameInstitution", institution.Name);
                    SqlParameter paramAddres = new SqlParameter(@"Addres", institution.Address);


                    dataCommand.Parameters.AddRange(new SqlParameter[] { paramId, paramActive, paramNameInstitution, paramAddres});
                    dataCommand.ExecuteNonQuery();

 
                }
                catch (Exception)
                {

                }
            }

        }
    }
}
