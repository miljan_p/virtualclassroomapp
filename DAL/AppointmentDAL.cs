﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.model;
using VirtualClassroomApp.utility;

namespace VirtualClassroomApp.DAL
{
    public class AppointmentDAL
    {

      private const string ConnectionString = "Data Source=DESKTOP-3OB8NQ2\\SQLEXPRESS;" +
      "                                     Integrated Security = True; Connect Timeout = 30; " +
      "                                     Encrypt=False;TrustServerCertificate=False;" +
      "                                     ApplicationIntent=ReadWrite;MultiSubnetFailover=False;Initial Catalog=Virtual Classroom;";

       public static Dictionary<int, Transformable> LoadAllAppointments()
        {

            Dictionary<int, Transformable> appointments = new Dictionary<int, Transformable>();

            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = connection.CreateCommand();
                var dataAdapter = new SqlDataAdapter();
                command.CommandText = "SELECT * FROM Appointment";
                dataAdapter.SelectCommand = command;
                var dataSet = new DataSet();

                dataAdapter.Fill(dataSet, "Appointment");


                foreach (DataRow dataRow in dataSet.Tables["Appointment"].Rows)
                {
                    var appointment = new Appointment();

                    appointment.SetId((int)dataRow["Id"]);
                    appointment.Active = (bool)dataRow["Active"];

                    appointment.Period = (Period) DataObjectMapper.periods[(int)dataRow["PeriodId"]];

                    appointment.Classroom = (Classroom)DataObjectMapper.classrooms[(int)dataRow["ClassroomId"]];

                    appointment.GroupName = "Classroom number: " + appointment.Classroom.ClassroomNumber.ToString();
                    appointment.StartDate = appointment.Period.Start;
                    appointment.TimeSpan = appointment.Period.End.Subtract(appointment.Period.Start);

                    switch ((int)dataRow["TeachingForm"])
                    {
                        case 0: appointment.SetTeachingForm(model.TeachingForm.Lecture); break;
                        case 1: appointment.SetTeachingForm(model.TeachingForm.Exercise); break;
                        default: appointment.SetTeachingForm(model.TeachingForm.Unspecified); break; //-1

                    }
                    int userId = (int)dataRow["UserId"];

                    if (DataObjectMapper.assistants.ContainsKey(userId)) appointment.User = (Assistant)DataObjectMapper.assistants[userId];
                    else appointment.User = (Professor)DataObjectMapper.professors[userId];

                    appointments.Add(appointment.GetId(), (Transformable)appointment);

                }

            }
                
            return appointments;
       }

        public static void UpdateAppointment(Appointment appointment)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand dataCommand = new SqlCommand();
                    dataCommand.Connection = connection;

                    dataCommand.CommandText = @"UPDATE Appointment SET Active=@Active, ClassroomId=@ClassroomId, TeachingForm=@TeachingForm, UserId=@UserId  WHERE Id=@Id";

                    SqlParameter paramId = new SqlParameter("@Id", appointment.GetId());
                    SqlParameter paramActive = new SqlParameter("@Active", Convert.ToInt32(appointment.Active));
                    SqlParameter paramClassroomId = new SqlParameter("@ClassroomId", appointment.Classroom.GetId());
                    SqlParameter paramTeachingForm = new SqlParameter(@"TeachingForm", (int)appointment.GetTeachingForm());
                    SqlParameter paramUserId = new SqlParameter(@"UserId", appointment.User.GetId());


                    dataCommand.Parameters.AddRange(new SqlParameter[] { paramId, paramActive, paramClassroomId, paramTeachingForm, paramUserId });
                    dataCommand.ExecuteNonQuery();


                    SqlCommand dataCommand2 = new SqlCommand();
                    dataCommand2.Connection = connection;
                    dataCommand2.CommandText = @"UPDATE Period SET Id=@Id, Active=@Active, StartDate=@StartDate, EndDate=@EndDate WHERE Id=@Id";

                    SqlParameter paramPeriodId = new SqlParameter("@Id", appointment.Period.GetId());
                    SqlParameter paramPeriodActive = new SqlParameter("@Active", Convert.ToInt32(appointment.Period.Active));
                    SqlParameter paramPeriodStartDate = new SqlParameter("@StartDate", Utility.convertDateWithTimeToString(appointment.Period.Start));
                    SqlParameter paramPeriodEndDate = new SqlParameter(@"EndDate", Utility.convertDateWithTimeToString(appointment.Period.End));

                    dataCommand2.Parameters.AddRange(new SqlParameter[] { paramPeriodId, paramPeriodActive, paramPeriodStartDate, paramPeriodEndDate });
                    dataCommand2.ExecuteNonQuery();


                }
                catch (Exception)
                {

                }


            }
        }


        public static void AddAppointment(Appointment appointment)
        {

            using (var connection = new SqlConnection(ConnectionString))
            {
                



                    connection.Open();


                    SqlCommand dataCommand2 = new SqlCommand();
                    dataCommand2.Connection = connection;
                    dataCommand2.CommandText = @"INSERT INTO Period "
                                             + "(Id, Active, StartDate, EndDate) "
                                               + "VALUES ( @Id, @Active, @StartDate, @EndDate)";

                    SqlParameter paramPeriodIdP = new SqlParameter("@Id", appointment.Period.GetId());
                    SqlParameter paramPeriodActive = new SqlParameter("@Active", Convert.ToInt32(appointment.Period.Active));
                    SqlParameter paramPeriodStartDate = new SqlParameter("@StartDate", Utility.convertDateWithTimeToString(appointment.Period.Start));
                    SqlParameter paramPeriodEndDate = new SqlParameter(@"EndDate", Utility.convertDateWithTimeToString(appointment.Period.End));

                    dataCommand2.Parameters.AddRange(new SqlParameter[] { paramPeriodIdP, paramPeriodActive, paramPeriodStartDate, paramPeriodEndDate });
                    dataCommand2.ExecuteNonQuery();



                    SqlCommand dataCommand = new SqlCommand();
                    dataCommand.Connection = connection;

                    dataCommand.CommandText = @"INSERT INTO Appointment "
                                             + "(Id, Active, PeriodId, ClassroomId, TeachingForm, UserId) "
                                               + "VALUES (@Id, @Active, @PeriodId, @ClassroomId, @TeachingForm, @UserId)";

                    SqlParameter paramId = new SqlParameter("@Id", appointment.GetId());
                    SqlParameter paramActive = new SqlParameter("@Active", Convert.ToInt32(appointment.Active));
                    SqlParameter paramPeriodIdA = new SqlParameter("@PeriodId", appointment.Period.GetId());
                    SqlParameter paramClassroomId = new SqlParameter("@ClassroomId", appointment.Classroom.GetId());
                    SqlParameter paramTeachingForm = new SqlParameter(@"TeachingForm", (int) appointment.GetTeachingForm());
                    SqlParameter paramUserId = new SqlParameter(@"UserId", appointment.User.GetId());


                    dataCommand.Parameters.AddRange(new SqlParameter[] { paramId, paramActive, paramPeriodIdA, paramClassroomId, paramTeachingForm, paramUserId });
                    dataCommand.ExecuteNonQuery();







             


            }

        }



    }

}
