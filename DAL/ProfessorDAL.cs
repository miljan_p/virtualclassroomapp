﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.DAL
{
    public class ProfessorDAL
    {
        private const string ConnectionString = "Data Source=DESKTOP-3OB8NQ2\\SQLEXPRESS;" +
           "                                     Integrated Security = True; Connect Timeout = 30; " +
           "                                     Encrypt=False;TrustServerCertificate=False;" +
           "                                     ApplicationIntent=ReadWrite;MultiSubnetFailover=False;Initial Catalog=Virtual Classroom;";

        public static Dictionary<int, Transformable> LoadAllProfessors()
        {
       

            Dictionary<int, Transformable> professors = new Dictionary<int, Transformable>();


            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = connection.CreateCommand();
                var dataAdapter = new SqlDataAdapter();
                command.CommandText = "select r.Id, r.Active, r.FirstName, r.LastName, r.Email, p.Institution, r.Username, r.Passwordd from RegisteredUser r, Professor p where r.Id = p.Id";
                dataAdapter.SelectCommand = command;
                var dataSet = new DataSet();

                dataAdapter.Fill(dataSet, "Professor");

                foreach (DataRow dataRow in dataSet.Tables["Professor"].Rows)
                {
                    var professor = new Professor();
                    professor.SetId((int)dataRow["Id"]);
                    professor.Active = (bool)dataRow["Active"];
                    professor.Name = (string)dataRow["FirstName"];
                    professor.LastName = (string)dataRow["LastName"];
                    professor.Email = (string)dataRow["Email"];
                    professor.Institution = (int)dataRow["Institution"];
                    professor.Username = (string)dataRow["Username"];
                    professor.Password = (string)dataRow["Passwordd"];
                    professor.Assistants = DataObjectMapper.FindAssistantsForProfessor(professor.GetId());
                    

                    professors.Add(professor.GetId(), (Transformable)professor);

                }
            }
            return professors;
        } //od metode

        public static void UpdateProfessor(Professor professor)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand dataCommand1 = new SqlCommand();
                    dataCommand1.Connection = connection;

                    dataCommand1.CommandText = @"UPDATE RegisteredUser SET Active=@Active, FirstName=@FirstName, LastName = @LastName, Email = @Email, Username = @Username, Passwordd = @Passwordd WHERE Id=@Id";

                    SqlParameter paramId = new SqlParameter("@Id", professor.GetId());
                    SqlParameter paramActive = new SqlParameter("@Active", Convert.ToInt32(professor.Active));
                    SqlParameter paramFirstName = new SqlParameter("@FirstName", professor.Name);
                    SqlParameter paramLastName = new SqlParameter(@"LastName", professor.LastName);
                    SqlParameter paramEmail = new SqlParameter(@"Email", professor.Email);
                    SqlParameter paramUsername = new SqlParameter(@"Username", professor.Username);
                    SqlParameter paramPasswordd = new SqlParameter(@"Passwordd", professor.Password);

                    dataCommand1.Parameters.AddRange(new SqlParameter[] { paramId, paramActive, paramFirstName, paramLastName, paramEmail, paramUsername, paramPasswordd });
                    dataCommand1.ExecuteNonQuery();

                    //NEMA STA DA SETUJEM U TABELI PROFESSOR
                    //SqlCommand dataCommand2 = new SqlCommand();
                    //dataCommand2.Connection = connection;
                    //dataCommand2.CommandText = "UPDATE Professor SET Professor=@Professor where Id=@Id";
                    //SqlParameter paramId2 = new SqlParameter("@Id", assistant.GetId());
                    //SqlParameter paramProfessor = new SqlParameter("@Professor", assistant.Professor);
                    //dataCommand2.Parameters.AddRange(new SqlParameter[] { paramId2, paramProfessor });
                    //dataCommand2.ExecuteNonQuery();
                }
                catch (Exception)
                {

                }


            }

        }


        public static void AddProfessor(Professor professor)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand dataCommand1 = new SqlCommand();
                    dataCommand1.Connection = connection;

                    dataCommand1.CommandText = @"INSERT INTO RegisteredUser "
                        + "( Active, FirstName, LastName, Email, Username, Passwordd, UserType) "
                        + "VALUES ( @Active, @FirstName, @LastName, @Email, @Username, @Passwordd, @UserType); SELECT SCOPE_IDENTITY()";


                    SqlParameter paramActive = new SqlParameter("@Active", Convert.ToInt32(professor.Active));
                    SqlParameter paramFirstName = new SqlParameter("@FirstName", professor.Name);
                    SqlParameter paramLastName = new SqlParameter(@"LastName", professor.LastName);
                    SqlParameter paramEmail = new SqlParameter(@"Email", professor.Email);
                    SqlParameter paramUsername = new SqlParameter(@"Username", professor.Username);
                    SqlParameter paramPasswordd = new SqlParameter(@"Passwordd", professor.Password);
                    SqlParameter paramUserType = new SqlParameter(@"UserType ", "Pr");

                    dataCommand1.Parameters.AddRange(new SqlParameter[] { paramActive, paramFirstName, paramLastName, paramEmail, paramUsername, paramPasswordd, paramUserType });

                    int id = Convert.ToInt32(dataCommand1.ExecuteScalar());
                    //Console.WriteLine(id);
                    professor.SetId(id);

                    
                    SqlCommand dataCommand2 = new SqlCommand();
                    dataCommand2.Connection = connection;
                    dataCommand2.CommandText = @"INSERT INTO Professor "
                                             + "(Id, Institution) "
                                               + "VALUES ( @Id, @Institution)";

                    SqlParameter paramId2 = new SqlParameter("@Id", professor.GetId());
                    SqlParameter paramInstitution = new SqlParameter("@Institution", professor.Institution);

                    dataCommand2.Parameters.AddRange(new SqlParameter[] { paramId2, paramInstitution });
                    dataCommand2.ExecuteNonQuery();
                }
                catch (Exception)
                {

                }


            }


        } //od metode









    } //od klase
}
