﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.DAL
{
    public class ClassroomDAL
    {
        private const string ConnectionString = "Data Source=DESKTOP-3OB8NQ2\\SQLEXPRESS;" +
          "                                     Integrated Security = True; Connect Timeout = 30; " +
          "                                     Encrypt=False;TrustServerCertificate=False;" +
          "                                     ApplicationIntent=ReadWrite;MultiSubnetFailover=False;Initial Catalog=Virtual Classroom;";

        public static Dictionary<int, Transformable> LoadAllClassrooms()
        {
            //from all institutions, (non)active...

            Dictionary<int, Transformable> classrooms = new Dictionary<int, Transformable>();


            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = connection.CreateCommand();
                var dataAdapter = new SqlDataAdapter();
                command.CommandText = "SELECT Id, Active, Institution, ClassroomNumber, Size, Computers FROM Classroom";
                dataAdapter.SelectCommand = command;
                var dataSet = new DataSet();

                dataAdapter.Fill(dataSet, "Classroom");

                foreach (DataRow dataRow in dataSet.Tables["Classroom"].Rows)
                {
                    var classroom = new Classroom();
                    
                    classroom.SetId((int)dataRow["Id"]);
                    classroom.Active = (bool)dataRow["Active"];
                    classroom.Institution = (int)dataRow["Institution"];
                    classroom.ClassroomNumber = (int)dataRow["ClassroomNumber"];
                    classroom.Size = (int)dataRow["Size"];
                    classroom.Computers = (bool)dataRow["Computers"];
                    


                    classrooms.Add(classroom.GetId(), (Transformable)classroom);

                }
            }
            return classrooms;
        }

        public static void AddClassroom(Classroom classroom)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand dataCommand = new SqlCommand();
                    dataCommand.Connection = connection;

                    dataCommand.CommandText = @"INSERT INTO Classroom "
                        + "(Id, Active, ClassroomNumber, Size, Computers, Institution) VALUES (@Id, @Active, @ClassroomNumber, @Size, @Computers, @Institution)"; // +   Active=@Active, NameInstitution=@NameInstitution, Addres = @Addres WHERE Id=@Id";

                    SqlParameter paramId = new SqlParameter("@Id", classroom.GetId());
                    SqlParameter paramActive = new SqlParameter("@Active", Convert.ToInt32(classroom.Active));
                    SqlParameter paramClassroomNumber = new SqlParameter("@ClassroomNumber", classroom.ClassroomNumber);
                    SqlParameter paramSize = new SqlParameter(@"Size", classroom.Size);
                    SqlParameter paramComputers = new SqlParameter(@"Computers", classroom.Computers);
                    SqlParameter paramInstitution = new SqlParameter(@"Institution", classroom.Institution);


                    dataCommand.Parameters.AddRange(new SqlParameter[] { paramId, paramActive, paramClassroomNumber, paramSize, paramComputers, paramInstitution });
                    dataCommand.ExecuteNonQuery();


                }
                catch (Exception)
                {

                }
            }
        }


        public static void UpdateClassroom(Classroom classroom)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand dataCommand = new SqlCommand();
                    dataCommand.Connection = connection;

                    dataCommand.CommandText = @"UPDATE Classroom SET Active=@Active, ClassroomNumber=@ClassroomNumber, Size = @Size, Computers = @Computers WHERE Id=@Id";

                    SqlParameter paramId = new SqlParameter("@Id", classroom.GetId());
                    SqlParameter paramActive = new SqlParameter("@Active", Convert.ToInt32(classroom.Active));
                    SqlParameter paramClassroomNumber = new SqlParameter("@ClassroomNumber", classroom.ClassroomNumber);
                    SqlParameter paramSize = new SqlParameter(@"Size", classroom.Size);
                    SqlParameter paramComputers = new SqlParameter(@"Computers", classroom.Computers);


                    dataCommand.Parameters.AddRange(new SqlParameter[] { paramId, paramActive, paramClassroomNumber, paramSize, paramComputers});
                    dataCommand.ExecuteNonQuery();

             
                }
                catch (Exception)
                {

                }


            }
        }


        public static bool IsUniqeClrNumber(int idClassroom, int idInstitution, int clrNumber)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {

                connection.Open();

                //SqlCommand dataCommand = new SqlCommand();
                //dataCommand.Connection = connection;
                var command = connection.CreateCommand();
                var dataAdapter = new SqlDataAdapter();
                command.CommandText = "SELECT Id, ClassroomNumber FROM Classroom WHERE Id != " + idClassroom.ToString() + " and Institution = " + idInstitution + " and ClassroomNumber = " + clrNumber.ToString();
                dataAdapter.SelectCommand = command;
                var dataSet = new DataSet();

                dataAdapter.Fill(dataSet, "Classroom");


                if (dataSet.Tables[0].Rows.Count == 0) return true;
                else return false;
                }
                catch (Exception)
                {

                }
                return false;
            }
        }//od metode
    }
}
