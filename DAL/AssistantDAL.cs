﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.DAL
{
    public class AssistantDAL
    {
        private const string ConnectionString = "Data Source=DESKTOP-3OB8NQ2\\SQLEXPRESS;" +
           "                                     Integrated Security = True; Connect Timeout = 30; " +
           "                                     Encrypt=False;TrustServerCertificate=False;" +
           "                                     ApplicationIntent=ReadWrite;MultiSubnetFailover=False;Initial Catalog=Virtual Classroom;";


        //public static Dictionary<int, Transformable> assistants = null;

        public static Dictionary<int, Transformable> LoadAllAssistnats()
        {
            //from all institutions, (non)active...

            Dictionary<int, Transformable> assistants = new Dictionary<int, Transformable>();


            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = connection.CreateCommand();
                var dataAdapter = new SqlDataAdapter();
                command.CommandText = "SELECT r.Id, r.Active, r.FirstName, r.LastName, r.Email, a.Institution, r.Username, r.Passwordd, a.Professor FROM RegisteredUser r, Assistant a 	WHERE r.Id = a.Id";
                dataAdapter.SelectCommand = command;
                var dataSet = new DataSet();

                dataAdapter.Fill(dataSet, "Assistant");

                foreach (DataRow dataRow in dataSet.Tables["Assistant"].Rows)
                {
                    var asistent = new Assistant();
                    asistent.SetId((int)dataRow["Id"]);
                    asistent.Active = (bool)dataRow["Active"];
                    asistent.Name = (string)dataRow["FirstName"];
                    asistent.LastName = (string)dataRow["LastName"];
                    asistent.Email = (string)dataRow["Email"];
                    asistent.Institution = (int)dataRow["Institution"];
                    asistent.Username = (string)dataRow["Username"];
                    asistent.Password = (string)dataRow["Passwordd"];

                    if (DBNull.Value.Equals(dataRow["Professor"]))
                    {   //dataRow["Professor"] == null
                        asistent.Professor = -1;
                    } else
                    {
                        asistent.Professor = (int)dataRow["Professor"];
                    }


                    

                    assistants.Add(asistent.GetId(), (Transformable)asistent);

                }
            }

            return assistants;

        } // od metode LoadAllAssistnats()

        public static void AddAssistant(Assistant assistant)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand dataCommand1 = new SqlCommand();
                    dataCommand1.Connection = connection;

                    //dataCommand1.CommandText = @"UPDATE RegisteredUser SET Active=@Active, FirstName=@FirstName, LastName = @LastName, Email = @Email, Username = @Username, Passwordd = @Passwordd WHERE Id=@Id";
                    dataCommand1.CommandText = @"INSERT INTO RegisteredUser "
                        + "( Active, FirstName, LastName, Email, Username, Passwordd, UserType) "
                        + "VALUES ( @Active, @FirstName, @LastName, @Email, @Username, @Passwordd, @UserType) SELECT SCOPE_IDENTITY()";

                   

                    SqlParameter paramActive = new SqlParameter("@Active", Convert.ToInt32(assistant.Active));
                    SqlParameter paramFirstName = new SqlParameter("@FirstName", assistant.Name);
                    SqlParameter paramLastName = new SqlParameter(@"LastName", assistant.LastName);
                    SqlParameter paramEmail = new SqlParameter(@"Email", assistant.Email);
                    SqlParameter paramUsername = new SqlParameter(@"Username", assistant.Username);
                    SqlParameter paramPasswordd = new SqlParameter(@"Passwordd", assistant.Password);
                    SqlParameter paramUserType = new SqlParameter(@"UserType ", "As");

                    dataCommand1.Parameters.AddRange(new SqlParameter[] { paramActive, paramFirstName, paramLastName, paramEmail, paramUsername, paramPasswordd, paramUserType });

                    int id = Convert.ToInt32(dataCommand1.ExecuteScalar());

                    assistant.SetId(id);


                    SqlCommand dataCommand2 = new SqlCommand();
                    dataCommand2.Connection = connection;
                    dataCommand2.CommandText = @"INSERT INTO Assistant "
                                             + "( Id, Professor, Institution) "
                                               + "VALUES ( @Id, @Professor, @Institution)";
                    SqlParameter paramId2 = new SqlParameter("@Id", assistant.GetId());

                    SqlParameter paramProfessor = null;
                    if (assistant.Professor != -1)
                    {
                        paramProfessor = new SqlParameter("@Professor", assistant.Professor);
                    }
                    else
                    {
                        paramProfessor = new SqlParameter("@Professor", DBNull.Value);
                    }

                    SqlParameter paramInstitution = new SqlParameter("@Institution", assistant.Institution);

                    dataCommand2.Parameters.AddRange(new SqlParameter[] { paramId2, paramProfessor, paramInstitution });
                    dataCommand2.ExecuteNonQuery();
                }
                catch (Exception)
                {

                }


            }

        }

        public static void UpdateAssistant(Assistant assistant)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand dataCommand1 = new SqlCommand();
                    dataCommand1.Connection = connection;

                    dataCommand1.CommandText = @"UPDATE RegisteredUser SET Active=@Active, FirstName=@FirstName, LastName = @LastName, Email = @Email, Username = @Username, Passwordd = @Passwordd WHERE Id=@Id";

                    SqlParameter paramId = new SqlParameter("@Id", assistant.GetId());
                    SqlParameter paramActive = new SqlParameter("@Active", Convert.ToInt32(assistant.Active));
                    SqlParameter paramFirstName = new SqlParameter("@FirstName", assistant.Name);
                    SqlParameter paramLastName = new SqlParameter(@"LastName", assistant.LastName);
                    SqlParameter paramEmail = new SqlParameter(@"Email", assistant.Email);
                    SqlParameter paramUsername = new SqlParameter(@"Username", assistant.Username);
                    SqlParameter paramPasswordd = new SqlParameter(@"Passwordd", assistant.Password);

                    dataCommand1.Parameters.AddRange(new SqlParameter[] { paramId, paramActive, paramFirstName, paramLastName, paramEmail, paramUsername, paramPasswordd });
                    dataCommand1.ExecuteNonQuery();

                    SqlCommand dataCommand2 = new SqlCommand();
                    dataCommand2.Connection = connection;
                    dataCommand2.CommandText = "UPDATE Assistant SET Professor=@Professor where Id=@Id";
                    SqlParameter paramId2 = new SqlParameter("@Id", assistant.GetId());

                    SqlParameter paramProfessor = null;
                    if (assistant.Professor != -1)
                    {
                        paramProfessor = new SqlParameter("@Professor", assistant.Professor);
                    }
                    else
                    {
                        paramProfessor = new SqlParameter("@Professor", DBNull.Value);
                    }

                    dataCommand2.Parameters.AddRange(new SqlParameter[] { paramId2, paramProfessor });
                    dataCommand2.ExecuteNonQuery();
                }
                catch (Exception)
                {

                }


            }

        }

        public static void UpdateAssistantsForProfessor(List<string> idAssistants)
        {
            //updaejtujemo asistente obrisanog profesora, tako sto cemo 
            //njihovo polje/atribut/obelezije "Professor" postaviti na -1

            using (var connection = new SqlConnection(ConnectionString))
            {

                try
                {
                    connection.Open();

                    SqlCommand dataCommand = new SqlCommand();
                    dataCommand.Connection = connection;
                    
                    dataCommand.CommandText = @"UPDATE Assistant SET Professor=@Professor WHERE Id in (" + createString(idAssistants) + ")";

                    SqlParameter paramProfessor = new SqlParameter("@Professor", DBNull.Value);

                    dataCommand.Parameters.AddRange(new SqlParameter[] { paramProfessor });
                    dataCommand.ExecuteNonQuery();




                }
                catch (Exception)
                {

                }


            }

        } //od metode

       

        private static string createString(List<string> idAssistants)
        {
           
            string[] array = idAssistants.ToArray();
            return String.Join(", ", array);
        }

    } //od klase AssistantDAL
} //OD NAMESPACA
