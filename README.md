Virtual Management Classroom

Student project GUI application in **C# WPF technology** with data persistance in **SQLServer** using **ADO.NET**.

Management and scheduling app for appointments accross multiple Institutions, where Admins, Professors and Assistants (TAs) are (un)scheduling Appointments.
App is equiped with **Telerik's WPF Schedule View Control**. Implemented all CRUD privileges.
