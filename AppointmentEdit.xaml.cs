﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.DAL;
using VirtualClassroomApp.DAO;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp
{
    /// <summary>
    /// Interaction logic for AppointmentEdit.xaml
    /// </summary>
    public partial class AppointmentEdit : Window
    {
        private RegisteredUser u;
        private Window sv;
        private Appointment appointment;
        private Appointment old;
        private int index;
       // private Classroom newSelectedClassroom;
        private DateTime startDate;
        private DateTime endDate;
        private DateTime originalStart;
        private DateTime originalEnd;
        private ObservableCollection<Classroom> allClassrooms;
        private ObservableCollection<Professor> allProfessors;
        private ObservableCollection<Assistant> allAssistants;
        private ObservableCollection<Appointment> appointments;

        public AppointmentEdit(Appointment appointment, Window sv, ObservableCollection<Appointment> appointments, Appointment old, int index)
        {
            
            InitializeComponent();
            this.index = index;
            this.sv = sv;
            this.old = old;
            this.appointment = appointment;
            this.appointments = appointments;
            this.allClassrooms = getClassrooms();
            this.allProfessors = getProfessors();
            this.allAssistants = getAssistants();

            pickerStartDate.DataContext = appointment.Period;
            pickerEndDate.DataContext = appointment.Period;

            originalStart = appointment.Period.Start;
            originalEnd = appointment.Period.End;

            dgClassrooms.DataContext = appointment;
            dgClassrooms.ItemsSource = allClassrooms;

            dgAssistants.DataContext = appointment;
            dgAssistants.ItemsSource = allAssistants;
           
            dgProfessors.DataContext = appointment;
            dgProfessors.ItemsSource = allProfessors;

        }

        

        private ObservableCollection<Classroom> getClassrooms()
        {
            return new ObservableCollection<Classroom>(DataObjectMapper.ClassroomsToList().FindAll(cl => cl.Active && cl.Institution == appointment.Classroom.Institution));
           
        }

        private ObservableCollection<Professor> getProfessors()
        {
            return new ObservableCollection<Professor>(DataObjectMapper.ProfessorsToList().FindAll(pr => pr.Active && pr.Institution == appointment.Classroom.Institution));
        }

        private ObservableCollection<Assistant> getAssistants()
        {
            return new ObservableCollection<Assistant>(DataObjectMapper.AssistantsToList().FindAll(assistant => assistant.Active && assistant.Institution == appointment.Classroom.Institution));
        }

        private void btnCheck_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                  
                var user = this.appointment.User.Name;
                var start = appointment.Period.Start.ToString();
                var end = appointment.Period.End.ToString();
                var classroom = appointment.Classroom.ClassroomNumber.ToString();
                MessageBox.Show("user: " + user + "\n" + "start: " + start + "\n" + "end: " + end + "\n" + "classroom number: " + classroom);

            }
            catch (Exception)
            {
                MessageBox.Show("AIFOJASOFJ");
            }
           
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {

            //if (appointment.User == null)
            //{
            //    MessageBox.Show("its a null");
            //}
            //
            //var user = this.appointment.User.Name;
            //var start = appointment.Period.Start.ToString();
            //var end = appointment.Period.End.ToString();
            //var classroom = appointment.Classroom.ClassroomNumber.ToString();
            //var id = appointment.GetId().ToString();
            //MessageBox.Show("appointment id: " + id + "\n" +"user: " + user + "\n" + "start: " + start + "\n" + "end: " + end + "\n" + "classroom number: " + classroom);
   
            //MessageBox.Show("start: " + pickerStartDate.SelectedValue.ToString() + "\n" + "end: " + pickerEndDate.SelectedValue.ToString());

            
            if(startDate >= endDate || endDate <= startDate)
            {
                //MessageBox.Show("start date: " + startDate.ToString() + "\n" + "end date: " + endDate.ToString());
                MessageBox.Show("Pick appropriate dates for appointment.");
                pickerStartDate.SelectedValue = originalStart;
                pickerEndDate.SelectedValue = originalEnd;
            }
            else if (!DataObjectMapper.avaliableWhileEditing(appointment.Classroom, new Period(-1, true, startDate, endDate), appointment.GetId(), this.appointment.User.GetId()))
            {
                MessageBox.Show("For choosen dates, classroom, and user, this appointment, overlaps with some other appointment.");
            }
            else
            {
                try
                {
                   
                    appointment.Period = new Period(appointment.Period.GetId(), appointment.Period.Active, startDate, endDate);

                    appointment.GroupName = "Classroom number: " + appointment.Classroom.ClassroomNumber.ToString();
                    appointment.StartDate = startDate;
                    appointment.TimeSpan = appointment.Period.End.Subtract(appointment.Period.Start);

                    if (appointment.User is Professor) appointment.SetTeachingForm(TeachingForm.Lecture);
                    else appointment.SetTeachingForm(TeachingForm.Exercise);

                    //FileTextIO.Edit(appointment, Appointment.PATH);
                    //FileTextIO.Edit(appointment.Period, Period.PATH);

                    MessageBox.Show("Appointment successfully edited");
                    sv.Close();
                    this.DialogResult = true;
                    AppointmentDAL.UpdateAppointment(appointment);
                    while (this.IsActive)
                    {
                        this.Close();
                    }
                    this.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("some error");
                }
               
                
                
            }

            
        }

        private void pickerStartDate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {      
            startDate = (DateTime)pickerStartDate.SelectedValue;
        }

        private void pickerEndDate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           endDate = (DateTime)pickerEndDate.SelectedValue;
        }

        private void btnCancle_Click(object sender, RoutedEventArgs e)
        {
            //this.Close();
            this.appointments[index] = old;
            //this.DialogResult = false;
            //sv.Close();
            this.Close();
        }


        private void dgAssistants_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
             
                if (dgAssistants.SelectedItem != null)
                {
                        dgProfessors.SelectedIndex = -1;
                }
   
                    
        }

        private void dgProfessors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            if (dgProfessors.SelectedItem != null)
            {
               dgAssistants.SelectedIndex = -1;

            }

        }

        private void dgClassrooms_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
