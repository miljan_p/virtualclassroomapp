﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.DAL;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp
{
    /// <summary>
    /// Interaction logic for ProfessorWindow.xaml
    /// </summary>
    public partial class ProfessorWindow : Window
    {
        private ICollectionView assistantsView;
        private ICollectionView avaliableAssistantsView;

        private Professor professor;
        private Institution institution;
        private ObservableCollection<Appointment> appointments;
        private ObservableCollection<Assistant> assistants;
        private ObservableCollection<Assistant> avaliableAssistants;
        public ProfessorWindow(Professor professor)
        {
            InitializeComponent();
            
            this.professor = professor;
            this.institution = (Institution) DataObjectMapper.institutions[professor.Institution]; 
            this.appointments =new ObservableCollection<Appointment>(DataObjectMapper.AppointmentsToList()
                                                                                    .Where(appointment => appointment.Active &&
                                                                                    appointment.Classroom.Institution == institution.GetId() &&
                                                                                    appointment.User.GetId() == professor.GetId()));
            dgProfessor.DataContext = professor;
            dgProfessor.ItemsSource = new ObservableCollection<Professor>() { professor };

            assistants = new ObservableCollection<Assistant>(professor.Assistants);
            this.avaliableAssistants = new ObservableCollection<Assistant>(DataObjectMapper.AssistantsToList()
                                                                                .Where(a => a.Active &&
                                                                                a.Professor == -1 && a.Institution == professor.Institution));

            dgAssistants.DataContext = assistants;
            assistantsView = CollectionViewSource.GetDefaultView(assistants);
            dgAssistants.ItemsSource = assistantsView;
            dgAssistants.IsSynchronizedWithCurrentItem = true;

            dgAvaliableAssistants.DataContext = avaliableAssistants;
            avaliableAssistantsView = CollectionViewSource.GetDefaultView(avaliableAssistants);
            dgAvaliableAssistants.ItemsSource = avaliableAssistantsView;
            dgAvaliableAssistants.IsSynchronizedWithCurrentItem = true;


        }

        private void btnScheduleView_Click(object sender, RoutedEventArgs e)
        {
            if (pickerStartDate.Value < pickerEndDate.Value)
            {
                ScheduleView sc = new ScheduleView(institution, (DateTime)pickerStartDate.Value, (DateTime)pickerEndDate.Value, appointments, professor, null);
                
                sc.Show();
                
            } else
            {
                MessageBox.Show("Starting date have to be greater then ending date.");
            }
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            Assistant selectedAssistant = (Assistant)dgAssistants.SelectedItem;

            if (selectedAssistant != null)
            {
                selectedAssistant.Professor = -1;
                //FileTextIO.Edit((Transformable)selectedAssistant, Assistant.PATH);
                AssistantDAL.UpdateAssistant(selectedAssistant);
                this.professor.Assistants.Remove(selectedAssistant);
                assistants.Remove(selectedAssistant);
                if (selectedAssistant.Active) avaliableAssistants.Add(selectedAssistant);
                MessageBox.Show("Selected assistant is removed from list of assistants.");
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Assistant selectedAvaliableAssistant = (Assistant)dgAvaliableAssistants.SelectedItem;

            if (selectedAvaliableAssistant != null)
            {
                selectedAvaliableAssistant.Professor = this.professor.GetId();
                this.professor.Assistants.Add(selectedAvaliableAssistant);
                // FileTextIO.Edit((Transformable)selectedAvaliableAssistant, Assistant.PATH);
                AssistantDAL.UpdateAssistant(selectedAvaliableAssistant);
                avaliableAssistants.Remove(selectedAvaliableAssistant);
                assistants.Add(selectedAvaliableAssistant);
                MessageBox.Show("Selected assistant is successfuly added to list of assistant of this professor.");
            }
        }

        private void btn_Logout(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
