﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.comparators.classroom_comaparators
{
    public class ClassroomComputersComparator : IComparer<Classroom>
    {
        private Order direction;
        public ClassroomComputersComparator(Order direction)
        {
            this.direction = direction;
        }

        public int Compare(Classroom x, Classroom y)
        {
            if (x != null && y != null)
            {
                Classroom classroom1 = x;
                Classroom classroom2 = y;


                return (int)this.direction * classroom1.Computers.CompareTo(classroom2.Computers);

            }
            else throw new ArgumentNullException("Objects cannot be compared!");
        }
    }
}

