﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.comparators.classroom_comaparators
{
    public class IdClassroomComparator : IComparer<Classroom>
    {
        private Order direction;
        public IdClassroomComparator(Order direction)
        {
            this.direction = direction;
        }

        public int Compare(Classroom x, Classroom y)
        {
            if (x != null && y != null)
            {
                Classroom classroom1 = x;
                Classroom classroom2 = y;

                if (classroom1.GetId() < classroom2.GetId()) return -1 * (int)this.direction;
                else if (classroom1.GetId() > classroom2.GetId()) return 1 * (int)this.direction;
                else return 0;

            }
            else throw new ArgumentNullException("Objects cannot be compared!");
        }
    }
}
