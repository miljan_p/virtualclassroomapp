﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.comparators.comparators_for_institutions
{
    public class AddressInstitutionComparator : IComparer<Institution>
    {
        private Order direction;

        public AddressInstitutionComparator(Order direction)
        {
            this.direction = direction;
        }

        public int Compare(Institution x, Institution y)
        {
            if (x != null && y != null)
            {
                Institution institution1 = x;
                Institution institution2 = y;


                return (int)this.direction * String.Compare(institution1.Address, institution2.Address);

            }
            else throw new ArgumentNullException("Objects cannot be compared!");
        }
    }
}
