﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.comparators.comparators_for_institutions
{
    public class IdInstitutionComparator : IComparer<Institution>
    {
        private Order direction;
        public IdInstitutionComparator(Order direction)
        {
            this.direction = direction;
        }

        public int Compare(Institution x, Institution y)
        {
            if (x != null && y != null)
            {
                Institution institution1 = x;
                Institution institution2 = y;

                if (institution1.GetId() < institution2.GetId()) return -1 * (int)this.direction;
                else if (institution1.GetId() > institution2.GetId()) return 1 * (int)this.direction;
                else return 0;

            }
            else throw new ArgumentNullException("Objects cannot be compared!");
        }
    }
}
