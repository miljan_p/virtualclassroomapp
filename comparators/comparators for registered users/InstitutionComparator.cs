﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.comparators
{
    public class InstitutionComparator : IComparer<RegisteredUser>
    {
        private Order direction;
        public InstitutionComparator(Order direction)
        {
            this.direction = direction;
        }

        public int Compare(RegisteredUser x, RegisteredUser y)
        {
            if (x != null && y != null)
            {
                RegisteredUser user1 = (RegisteredUser)x;
                RegisteredUser user2 = (RegisteredUser)y;

                // if (user1.Institution < user2.Institution) return -1 * (int)this.direction;
                //else if (user1.Institution > user2.Institution) return 1 * (int)this.direction;
                // else return 0;
                return 0;

            }
            else throw new ArgumentNullException("Objects cannot be compared!");
        }
    }
}
