﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.comparators
{
    public class ActiveComparator : IComparer<RegisteredUser>
    {
        private Order direction;
        public ActiveComparator(Order direction)
        {
            this.direction = direction;
        }

        public int Compare(RegisteredUser x, RegisteredUser y)
        {
            if (x != null && y != null)
            {
                RegisteredUser user1 = (RegisteredUser)x;
                RegisteredUser user2 = (RegisteredUser)y;

                return (int)this.direction * user1.Active.CompareTo(user2.Active);

            }
            else throw new ArgumentNullException("Objects cannot be compared!");
        }

    }
}
