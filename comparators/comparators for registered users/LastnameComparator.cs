﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.comparators
{
    public class LastnameComparator : IComparer<RegisteredUser>
    {
        private Order direction;

        public LastnameComparator(Order direction)
        {
            this.direction = direction;
        }

        public int Compare(RegisteredUser x, RegisteredUser y)
        {
            if (x != null && y != null)
            {
                RegisteredUser user1 = (RegisteredUser)x;
                RegisteredUser user2 = (RegisteredUser)y;


                return (int)this.direction * String.Compare(user1.Name, user2.Name);

            }
            else throw new ArgumentNullException("Objects cannot be compared!");
        }
    }
}
