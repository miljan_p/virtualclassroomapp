﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;

namespace VirtualClassroomApp.DAO
{
    public class FileUtils
    {


        public static StreamReader GetTxtReader(string fileName)
        {
            var fileStream = new FileStream(GetTxtPath(fileName), FileMode.Open, FileAccess.Read);
            return new StreamReader(fileStream, Encoding.UTF8);
        }

        public static StreamWriter GetTxtWriter(string fileName)
        {
            FileStream fileStream = new FileStream(GetTxtPath(fileName), FileMode.Append);
            return new StreamWriter(fileStream, Encoding.UTF8);
        }


        public static FileStream GetBinaryStreamWriter(string fileName)
        {
            return new FileStream(GetBinPath(fileName), FileMode.Append, FileAccess.Write, FileShare.ReadWrite);

        }

        public static FileStream GetBinaryStreamReader(string fileName)
        {
            return new FileStream(GetBinPath(fileName), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

        }



        public static string GetTxtPath(string fileName)
        {
            var startDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            var absPath = startDirectory + @"\data\" + fileName;
            
            return absPath;
        }


        public static string GetBinPath(string fileName)
        {
            var startDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            var absPath = startDirectory + @"\data serializable\" + fileName;
            return absPath;
        }

    }
}
