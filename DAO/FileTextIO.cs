﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.DAO
{
    public class FileTextIO
    {

        public static Dictionary<int, Transformable> Load(Transformable o, string fileName)
        {
            Dictionary<int, Transformable> mapa = new Dictionary<int, Transformable>();
            try
            {
                using (StreamReader reader = FileUtils.GetTxtReader(fileName))
                {
                    String line;

                    while ((line = reader.ReadLine()) != null)
                    {

                        if (!line.Equals(""))
                        {
                            Transformable newTransformable = o.ReadFromFile(line);
                            mapa.Add(newTransformable.GetId(), newTransformable);
                        }
                        else
                        {
                            continue;
                        }
                    }
                }

            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine($"The directory was not found: '{e}'");
            }
            catch (IOException e)
            {
                Console.WriteLine($"The file could not be opened: '{e}'");
            }

            return mapa;
        }

        public static void Add(Transformable o, string fileName)
        {

            try
            {
                using (StreamWriter writer = FileUtils.GetTxtWriter(fileName))
                {
                    writer.Write(o.WriteToFile());
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine($"The file was not found: '{e}'");
            }
            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine($"The directory was not found: '{e}'");
            }
            catch (IOException e)
            {
                Console.WriteLine($"The file could not be opened: '{e}'");
            }

        }

        public static void Edit(Transformable o, string fileName)
        {
            try
            {
                var originalFile = FileUtils.GetTxtPath(fileName);
                var tempFile = FileUtils.GetTxtPath("temp.txt");

                using (var readStream = File.OpenRead(originalFile))
                using (var writeStream = File.OpenWrite(tempFile))
                using (var reader = new StreamReader(readStream))
                using (var writer = new StreamWriter(writeStream))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (!line.Equals(""))
                        {
                            if (o.GetId() == getIdFromString(line))
                            {
                                writer.WriteLine(o.WriteToFile());
                            }
                            else
                            {
                                writer.WriteLine(line);
                            }
                        }
                    }
                }

                File.Delete(originalFile);
                File.Move(tempFile, originalFile);
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine($"The file was not found: '{e}'");
            }
            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine($"The directory was not found: '{e}'");
            }
            catch (IOException e)
            {
                Console.WriteLine($"The file could not be opened: '{e}'");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error while reading/writing from/to file(s): '{e}'");
            }


        }

        public static void Delete(Transformable o, string fileName)
        {
            try
            {
                var originalFile = FileUtils.GetTxtPath(fileName);
                var tempFile = FileUtils.GetTxtPath("temp.txt");

                using (var readStream = File.OpenRead(originalFile))
                using (var writeStream = File.OpenWrite(tempFile))
                using (var reader = new StreamReader(readStream))
                using (var writer = new StreamWriter(writeStream))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (!line.Equals(""))
                        {
                            if (o.GetId() == getIdFromString(line))
                            {
                                writer.WriteLine(o.WriteToFile()+"\n");
                            }
                            else
                            {
                                writer.WriteLine(line);
                            }
                        }
                    }
                }

                File.Delete(originalFile);
                File.Move(tempFile, originalFile);
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine($"The file was not found: '{e}'");
            }
            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine($"The directory was not found: '{e}'");
            }
            catch (IOException e)
            {
                Console.WriteLine($"The file could not be opened: '{e}'");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error while reading/writing from/to file(s): '{e}'");
            }


        }





        private static int getIdFromString(string line)
    {

            return Int32.Parse(line.Split('|')[0]);

    }










    }
}
