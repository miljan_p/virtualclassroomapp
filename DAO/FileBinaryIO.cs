﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp.DAO
{
    public class FileBinaryIO
    {

        public static Dictionary<int, Transformable> Deserialize(string fileName)
        {
            Dictionary<int, Transformable> dictionary = new Dictionary<int, Transformable>();

            using (FileStream fileStream = FileUtils.GetBinaryStreamReader(fileName))
            {
                BinaryFormatter deserializer = new BinaryFormatter();

                while (fileStream.Position < fileStream.Length)
                {
                    Transformable t = (Transformable)deserializer.Deserialize(fileStream);
                    dictionary.Add(t.GetId(), t);
                }

                fileStream.Close();
            }
            return dictionary;
        }


        public static void Serialize(Transformable obj, string fileName)
        {

            IFormatter formatter = new BinaryFormatter();

            formatter.Serialize(FileUtils.GetBinaryStreamWriter(fileName), obj);


        }







    }
}
