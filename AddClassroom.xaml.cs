﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp
{
    /// <summary>
    /// Interaction logic for AddClassroom.xaml
    /// </summary>
    public partial class AddClassroom : Window
    {
        private Classroom newClassroom;
        private static int ins;
        public AddClassroom(Classroom newClassroom)
        {
            InitializeComponent();
            this.newClassroom = newClassroom;
            ins = newClassroom.Institution;
            tbSize.DataContext = newClassroom;
            tbClassroomNumber.DataContext = newClassroom;
            chbComputers.IsChecked = newClassroom.Computers;
            chbComputers.DataContext = newClassroom;
            tbInstitution.DataContext = newClassroom;
 
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            //debug poruka
            //MessageBox.Show("size: " + newClassroom.Size + "\n" + "classroom number: " + newClassroom.ClassroomNumber.ToString() + "\n" + "computers: " + newClassroom.Computers.ToString());
            //uraditi validaciju pa onda setovati dialog result...            
            if (newClassroom.Size > 0 && newClassroom.ClassroomNumber != 0) this.DialogResult = true;
            else MessageBox.Show("Classroom size and classroom number must be greater then 0 and/or unique");

            //prakticno zatvaranje prozora za dodvanje classroom-a i vracanje na admin window
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        public static int idInstituion()
        {

            return ins;
                
                
        }
    }
}
