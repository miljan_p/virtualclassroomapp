﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.DAL;
using VirtualClassroomApp.DAO;
using VirtualClassroomApp.model;

namespace VirtualClassroomApp
{
    /// <summary>
    /// Interaction logic for EditProfessorForAssistant.xaml
    /// </summary>
    /// 
   
    public partial class EditProfessorForAssistant : Window
    {
        private Assistant assistant;
        private ObservableCollection<Professor> professor;
        private ObservableCollection<Professor> avaliableProfessors;

        private ICollectionView professorView;
        private ICollectionView avaliableAProfessorsView;

        public EditProfessorForAssistant(Assistant assistant)
        {
            InitializeComponent();
            this.assistant = assistant;

            //setovanje asistentovog profesora
            if (assistant.Professor == -1)
            {
                professor = new ObservableCollection<Professor>();
                avaliableProfessors = new ObservableCollection<Professor>(DataObjectMapper.ProfessorsToList()
                                                                               .Where(p => p.Active && assistant.Institution == p.Institution));
            } else
            {
                // Professor assistantsProfessor = DataObjectMapper.ProfessorsToList().Find(p => p.GetId() == assistant.Professor);
                Professor assistantsProfessor = (Professor) DataObjectMapper.professors[assistant.Professor];
                if (!assistantsProfessor.Active) professor = new ObservableCollection<Professor>();
                else
                {
                    professor = new ObservableCollection<Professor>();
                    professor.Add(assistantsProfessor);
                    avaliableProfessors = new ObservableCollection<Professor>(DataObjectMapper.ProfessorsToList()
                                                                               .Where(p => p.Active && assistant.Institution == p.Institution && p.GetId() != assistantsProfessor.GetId()));
                }

            }


           


            dgProfessor.DataContext = professor;
            professorView = CollectionViewSource.GetDefaultView(professor);
            dgProfessor.ItemsSource = professorView;
            dgProfessor.IsSynchronizedWithCurrentItem = true;


            dgAvaliableProfessors.DataContext = avaliableProfessors;
            avaliableAProfessorsView = CollectionViewSource.GetDefaultView(avaliableProfessors);
            dgAvaliableProfessors.ItemsSource = avaliableAProfessorsView;
            dgAvaliableProfessors.IsSynchronizedWithCurrentItem = true;






        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (professor.Count > 0)
            {
                MessageBox.Show("Assistant already have a professor.");
            } else
            {
                try
                {
                    Professor selectedProfessor = (Professor)dgAvaliableProfessors.SelectedItem;
                    professor.Add(selectedProfessor);
                    avaliableProfessors.Remove(selectedProfessor);
                    selectedProfessor.Assistants.Add(assistant);
                    assistant.Professor = selectedProfessor.GetId();
                    // FileTextIO.Edit((Transformable)assistant, Assistant.PATH); //Edit
                    AssistantDAL.UpdateAssistant(assistant);
                }
                catch (Exception) { }
               

            }
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Professor selectedProfessor = (Professor) dgProfessor.SelectedItem; //SELEKTUJ ASSISTENTOVOG  PROF IZ DATA
                professor.Remove(selectedProfessor); //ukloni ga iz dg
                selectedProfessor.Assistants.Remove(assistant); //iz liste assistenata tog profesora, izbaci ovog asistenta
                avaliableProfessors.Add(selectedProfessor); //dodaj uklonjenog (iz dgProfessor) profesora u dgAvailable
                assistant.Professor = -1; //setuj ASISTENTOVOG prof na -1, znaci nema vise izabranog prof
                                          // FileTextIO.Edit((Transformable)assistant, Assistant.PATH); //Edit
                AssistantDAL.UpdateAssistant(assistant);
                
            }
            catch (Exception) { }
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
