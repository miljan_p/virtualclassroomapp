﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls.Timeline;
using VirtualClassroomApp.controller;
using VirtualClassroomApp.DAL;
using VirtualClassroomApp.model;
using VirtualClassroomApp.utility;
using MessageBox = System.Windows.Forms.MessageBox;

namespace VirtualClassroomApp
{
    /// <summary>
    /// Interaction logic for ScheduleView.xaml
    /// </summary>
    public partial class ScheduleView : Window
    {
        public Institution selectedInstitution;
        public Appointment selectedAppointment;
        public Product product;
        public Guest gu;
        private RegisteredUser ru;
        private ObservableCollection<Appointment> appointments;
        
        public ScheduleView(Institution selectedInstitution,DateTime start, DateTime end, ObservableCollection<Appointment> appointments, RegisteredUser ru=null, Guest gu=null)
        {
            InitializeComponent();
            this.ru = ru;
            this.gu = gu;

            if (gu != null) btnAdd.Visibility = Visibility.Hidden;
            
            EventManager.RegisterClassHandler(typeof(TimelineItemControl), TimelineItemControl.MouseLeftButtonDownEvent, new RoutedEventHandler(MouseLeftButtonDownEvent));
            EventManager.RegisterClassHandler(typeof(TimelineItemControl), TimelineItemControl.MouseRightButtonDownEvent, new RoutedEventHandler(MouseRightButtonDownEvent));


            this.selectedInstitution = selectedInstitution;

            this.appointments = appointments;


            this.product = new Product(appointments, start, end);
           
            this.DataContext = product;

        }
       
        private void MouseLeftButtonDownEvent(object sender, RoutedEventArgs e)
        {

            try
            {
                if ((ru == null && gu == null))
                {
                    //full edit mode za admina
                    var clickedItem = sender as TimelineItemControl;
                    var timelineDataItem = clickedItem.DataContext as TimelineDataItem;
                    var appointment = timelineDataItem.DataItem as Appointment;
                    this.selectedAppointment = appointment;

                    Appointment old = (Appointment)selectedAppointment.Clone();
                    int index = appointments.IndexOf(selectedAppointment);
                    AppointmentEdit ae = new AppointmentEdit(appointment, this, appointments, old, index);
                    if (ae.ShowDialog() == true)
                    {
                        ae.Close();
                    }

                }
                else if (ru != null || gu != null)
                {
                    //obicni detalji o appointmentu
                    var clickedItem = sender as TimelineItemControl;
                    var timelineDataItem = clickedItem.DataContext as TimelineDataItem;
                    var appointment = timelineDataItem.DataItem as Appointment;
                    this.selectedAppointment = appointment;
                    MessageBox.Show("Details:" + "\n" + "Start: " + appointment.Period.Start.ToString() + "\n" + "End: " + appointment.Period.End.ToString() + "\n" + "Classroom number: " + appointment.Classroom.ClassroomNumber.ToString());

                }
            }
            catch (Exception) { };

           

        }

        private  void MouseRightButtonDownEvent(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((ru == null && gu == null) || (ru != null) ) && gu == null)
                {
                    //delete za admina ili ru
                    var clickedItem = sender as TimelineItemControl;
                    var timelineDataItem = clickedItem.DataContext as TimelineDataItem;
                    var appointment = timelineDataItem.DataItem as Appointment;
                    this.selectedAppointment = appointment;

                    MessageBoxResult result = (MessageBoxResult)MessageBox.Show("Do you want to remove this appointment", "Remove appointment", MessageBoxButtons.YesNo);

                    if (MessageBoxResult.Yes == result)
                    {
                        appointment.Active = false;
                        appointment.Period.Active = false;
                        AppointmentDAL.UpdateAppointment(appointment);
                        this.appointments.Remove(appointment);

                        MessageBox.Show("Appointment deleted.");
                        this.Close();
                    }

                } 
                
            }
            catch (Exception) { }

        
        }




        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            AddAppointmentAdmin aaa = new AddAppointmentAdmin(selectedInstitution.GetId(), appointments, ru);
            aaa.Show();
            
        }
    }
}
